module aleo

go 1.20

require (
	github.com/minio/blake2b-simd v0.0.0-20160723061019-3f5f724cb5b1
	github.com/robvanmieghem/go-opencl v0.0.0-20160201165807-5ca28f1a8220
	golang.org/x/crypto v0.16.0
)

require golang.org/x/sys v0.15.0 // indirect

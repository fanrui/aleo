/***
Translate from Aleo cuda code.
Copyright (c) 2023 Software, Inc.  All rights reserved.
Licensed under the Apache License, Version 1.0, see LICENSE for details.
Written by James(fanxu990516@gmail.com) And Yan Xiao.
***/

/** Parameter.h start */

// #define SM 68
#define testx

// main loop
#define Evaluations_size_ 8192

#define BIT_MAX 17
#define LEN 12
#define LEN_256 8

#define MODULUS_BITS_G 253
#define data_alloc_len_ 128
#define degree_ 8192

// initial part
#define Mbits 14

__constant int M_LEN=192;

__constant  uint core_per_sm = 1; // 外部调用值保持一致 可用 {{core_per_sm}} 替换字符串方式 再加载内核的时候给替换初始化掉，如果是固定的可以随时更换此值
__constant  uint threads_to_use = 0;
#if USE_VAR == 1
__constant  uint device_bits = 0;
__constant  uint device_bits_size = 0;
__constant  uint device_size = 0;
__constant  uint device_evaluation_size = 0;
#endif
// constant.h end
__constant uint MAX = 0xFFFFFFFF;
__constant ulong MAX64 = 0xFFFFFFFFFFFFFFFF;

/** Parameter.h end */

/** swj_compute.h start */

typedef struct PROJECTIVE384 {
  uint x[12];
  uint y[12];
  uint z[12];
} projective384;

typedef struct AFFINE384 {
  uint x[12];
  uint y[12];
} affine384;

typedef struct _PROJECTIVE384_XYZZ
{
  uint x[12];
  uint y[12];
  uint zz[12];
  uint zzz[12];
} projective384xyzz;

/** swj_compute.h end */

/***

Portions of this file originated in an NVIDIA Open Source Project.  The source can be
found here:  http://github.com/NVlabs/CGBN, from: include/cgbn/arith/asm.cu.

Copyright (c) 2022, Yrrid Software, Inc.  All rights reserved.
Licensed under the Apache License, Version 2.0, see LICENSE for details.

Written by Niall Emmart.

***/

// state context asm.cu start

typedef struct{
    /* whatever */
    int CC;
    bool firstOperation;
}  GlobalParameters ;

//__global  GlobalParameters globalParams = (GlobalParameters){0,false};

uint uadd_cc(uint a, uint b ,GlobalParameters *globalParams) {
    uint lo = a + b;
    globalParams->CC = lo < a;
    return lo;
}

 uint uaddc_cc(uint a, uint b,GlobalParameters *globalParams) {
  uint lo = a+b+globalParams->CC;
//   globalParams->CC = lo < a;
  globalParams->CC = (lo < a)||(lo==a && globalParams->CC==1);
  return lo;
}

 uint uaddc(uint a, uint b,GlobalParameters *globalParams) {
  uint lo = a+b+globalParams->CC;
  return lo;
}

 uint usub_cc(uint a, uint b,GlobalParameters *globalParams) {
  uint lo = a - b;
  globalParams->CC = lo <= a;
  return lo;
}

 uint usubc_cc(uint a, uint b,GlobalParameters *globalParams) {
//   printf("globalParams->CC:%u",globalParams->CC);
  uint lo = a - b - (1-globalParams->CC);
//   globalParams->CC = lo <= a;
  globalParams->CC = lo < a ||(lo==a && globalParams->CC==1);
  return lo;
}

 uint usubc(uint a, uint b,GlobalParameters *globalParams) {
  uint lo = a - b - (1-globalParams->CC);
  return lo;
}

 bool getCarry(GlobalParameters *globalParams) {
  return globalParams->CC;
}

 void setCarry(bool cc,GlobalParameters *globalParams) {
    globalParams->CC = cc;
}

 ulong mulwide(uint a, uint b) {
  ulong r = (ulong) a * (ulong) b;
  return r;
}

 ulong asmmadwide(uint a, uint b, ulong c) {
  ulong r = (ulong) a * (ulong) b + c;
  
  //asm volatile ("mad.wide.u32 %0,%1,%2,%3;" : "=l"(r) : "r"(a), "r"(b), "l"(c));
  return r;
}

 ulong madwide_cc(uint a, uint b, ulong c,GlobalParameters *globalParams) {
  ulong aa = (ulong) a * (ulong) b;
  ulong r = aa + c;
  globalParams->CC = r < aa;           
  return r;
}

 ulong madwidec_cc(uint a, uint b, ulong c,GlobalParameters *globalParams) {
  ulong aa = (ulong) a * (ulong) b;
  ulong r = aa + c + globalParams->CC;
//   globalParams->CC = r < aa;  
  globalParams->CC = r < aa ||(r==aa && globalParams->CC==1);         
  return r;
}

 ulong madwidec(uint a, uint b, ulong c,GlobalParameters *globalParams) {
  ulong aa = (ulong) a * (ulong) b;
  ulong r = aa + c + globalParams->CC;      
  return r;
}

 uint2 u2madwidec_cc(uint a, uint b, ulong c,GlobalParameters *globalParams) {
  ulong aa = (ulong) a * (ulong) b;
  ulong r = aa + c + globalParams->CC;
//   globalParams->CC = r < aa;
  globalParams->CC = r < aa ||(r==aa && globalParams->CC==1);
  uint2 f;
  f.x = (uint) r ; // lo
  f.y = (uint) (r >> 32); // hi
  return f;
}

 uint __attribute__((overloadable)) ulow(uint2 xy) {
  return xy.x;
}

 uint __attribute__((overloadable)) uhigh(uint2 xy) {
  return xy.y;
}

 uint __attribute__((overloadable)) ulow(ulong wide) {
  uint r = (uint) wide;
  return r;
}

 uint __attribute__((overloadable)) uhigh(ulong wide) {
  uint r = (uint) (wide >> 32);
  return r;
}

 ulong __attribute__((overloadable)) make_wide(uint lo, uint hi) {
  ulong r = (ulong) hi << 32 | lo;
  return r;
}

 ulong __attribute__((overloadable)) make_wide(uint2 xy) {
  return make_wide(xy.x, xy.y);
}

/** asm.cu end */


/***

Copyright (c) 2022, Yrrid Software, Inc.  All rights reserved.
Licensed under the Apache License, Version 2.0, see LICENSE for details.

Written by Niall Emmart. chain.cu start

***/
  
void __attribute__((overloadable)) chain_t(GlobalParameters *globalParams) {
    globalParams->firstOperation=true;
  }

void __attribute__((overloadable)) chain_t(bool carry,GlobalParameters *globalParams) {
    globalParams->firstOperation=false;
    // uadd_cc(carry ? 1 : 0, 0xFFFFFFFF,globalParams);
    if( carry ){
        globalParams->CC = 1;
    } else{
        globalParams->CC = 0;
    }
  }
  
   void __attribute__((overloadable)) reset(GlobalParameters *globalParams) {
    globalParams->firstOperation=true;
    // uadd_cc(0, 0,globalParams);
    globalParams->CC = 0;
  }

   void reset_(GlobalParameters *globalParams) {
    // uadd_cc(0, 0,globalParams);
    globalParams->CC = 0;
  }
  
   void __attribute__((overloadable)) reset(bool carry,GlobalParameters *globalParams) {
    globalParams->firstOperation=false;
    // uadd_cc(carry ? 1 : 0, 0xFFFFFFFF,globalParams);
    if( carry ){
        globalParams->CC = 1;
    } else{
        globalParams->CC = 0;
    }
  }
  
   uint add(uint a, uint b,GlobalParameters *globalParams) {
    if(globalParams->firstOperation) {
    //   uadd_cc(0, 0,globalParams);
      globalParams->CC = 0;
    }
    globalParams->firstOperation=false;
    return uaddc_cc(a, b,globalParams);
  }
  
   uint add_(uint a, uint b,GlobalParameters *globalParams) {
    return uaddc_cc(a, b,globalParams);
  }


   uint sub(uint a, uint b,GlobalParameters *globalParams) {
    if(globalParams->firstOperation){
    //   uadd_cc(1, 0xFFFFFFFF,globalParams);
      globalParams->CC = 1;
    }
    globalParams->firstOperation=false;
    return usubc_cc(a, b,globalParams);
  }
  
   uint2 u2madwide(uint a, uint b, uint2 c,GlobalParameters *globalParams) {
    if(globalParams->firstOperation) {
        // uadd_cc(0, 0,globalParams);
        globalParams->CC = 0;
    }
    globalParams->firstOperation=false;    
    return u2madwidec_cc(a, b, make_wide(c.x,c.y),globalParams);
  }

   ulong madwide_(uint a, uint b, ulong c,GlobalParameters *globalParams) {
    return madwidec_cc(a, b, c,globalParams);
  }

   ulong madwide(uint a, uint b, ulong c,GlobalParameters *globalParams) {
    if(globalParams->firstOperation) {
        // uadd_cc(0, 0,globalParams);
        globalParams->CC = 0;
    }
    globalParams->firstOperation=false;    
    return madwidec_cc(a, b, c,globalParams);
  }

   uint uhi(ulong c) {    
    return uhigh(c);
  }

  /** chain.cu end */

  /** chain.cu end */


  /** MP.cu start*/
__constant static uint zc=0;

 uint computeNP0(uint x) {
  uint inv=x;

  inv=inv*(inv*x+14);
  inv=inv*(inv*x+2);
  inv=inv*(inv*x+2);
  inv=inv*(inv*x+2);
  return inv;
}


void mp_zero(int limbs,uint* x) {
  
  for(int i=0;i<limbs;i++)
    x[i]=0;
}


 void mp_copy(int limbs,uint* dst, const uint* src) {
  
  for(int i=0;i<limbs;i++)
    dst[i]=src[i];
}


 ulong mp_madwide(uint a, uint b, ulong c) {
  return asmmadwide(a, b, c);
}


 uint mp_logical_or(int limbs,const uint* a) {
  uint lor=a[0];
  
  
  for(int i=1;i<limbs;i++)
    lor=lor | a[i];
  return lor;
}

 uint __funnelshift_rc ( uint  lo, uint  hi, uint  shift ) {
    ulong r = make_wide(lo,hi);
    if( shift < 32){
      r = r >> shift;
    } else{
      r = r >> 32;
    }
    return ulow(r);
 }
 uint __funnelshift_lc ( uint  lo, uint  hi, uint  shift ) {
  ulong r = make_wide(lo,hi);
    if( shift < 32){
      r = r << shift;
    } else{
      r = r << 32;
    }
    return uhigh(r);
 }
 
 uint mp_shift_right(int limbs,uint* r, const uint* x, uint bits) {
  
  for(int i=0;i<limbs-1;i++)
    r[i]=__funnelshift_rc(x[i], x[i+1], bits);
  r[limbs-1]=__funnelshift_rc(x[limbs-1], 0, bits);
}


 uint mp_shift_left(int limbs,uint* r, const uint* x, uint bits) {
  
  for(int i=limbs-1;i>0;i--)
    r[i]=__funnelshift_lc(x[i-1], x[i], bits);
  r[0]=__funnelshift_lc(0, x[0], bits);
}


 void mp_add(int limbs,uint* r, const uint* a, const uint* b) {
  GlobalParameters globalParams;
  chain_t(&globalParams);
  
  for(int i=0;i<limbs;i++)
    r[i]=add(a[i], b[i],&globalParams);
}


 bool mp_add_carry(int limbs,uint* r, const uint* a, const uint* b) {
  GlobalParameters globalParams;
  chain_t(&globalParams);
  
  
  for(int i=0;i<limbs;i++)
    r[i]=add(a[i], b[i],&globalParams);
  return getCarry(&globalParams);
}


 void mp_sub(int limbs,uint* r, const uint* a, const uint* b) {
  GlobalParameters globalParams;
  chain_t(&globalParams);
  
  
  for(int i=0;i<limbs;i++)
    r[i]=sub(a[i], b[i],&globalParams);
}


 bool mp_sub_carry(int limbs,uint* r, const uint* a, const uint* b) {
  GlobalParameters globalParams;
  chain_t(&globalParams);
  bool carry;
  reset(&globalParams);
  
  for(int i=0;i<limbs;i++) {
    r[i]=sub(a[i], b[i],&globalParams);
  } 
  return getCarry(&globalParams);
}


 bool mp_comp_eq(int limbs,const uint* a, const uint* b) {
  uint match=a[0] ^ b[0];
  
  
  for(int i=1;i<limbs;i++)
    match=match | (a[i] ^ b[i]);
  return match==0;
}


 bool mp_comp_ge(int limbs,const uint* a, const uint* b) {
  GlobalParameters globalParams;
  chain_t(&globalParams);
  
  
  for(int i=0;i<limbs;i++)
    sub(a[i], b[i],&globalParams);
  return getCarry(&globalParams);
}


 bool mp_comp_gt(int limbs,const uint* a, const uint* b) {
  GlobalParameters globalParams;
  chain_t(&globalParams);
  
  // a>b --> b-a is negative
  
  for(int i=0;i<limbs;i++)
    sub(b[i], a[i],&globalParams);
  return !getCarry(&globalParams);
}


 void mp_select(int limbs,uint* r, bool abSelect, const uint* a, const uint* b) {
  
  for(int i=0;i<limbs;i++) 
    r[i]=abSelect ? a[i] : b[i];
}


 bool mp_mul_red_cl(int limbs,ulong* evenOdd, const uint* a, const uint* b, const uint* n) {
  ulong* even=evenOdd;
  ulong* odd=evenOdd + limbs/2;
  GlobalParameters globalParams;
  chain_t(&globalParams);
  bool      carry=false;
  uint  lo=0, q, c1, c2;
  
  // This routine can be used when max(a, b)+n < R (i.e. it doesn't carry out).  Hence the name cl for carryless.
  // Only works with an even number of limbs.
#if 1    
  
  for(int i=0;i<limbs/2;i++) {
    even[i]=make_wide(0, 0);
    odd[i]=make_wide(0, 0);
  }
  
  
  for(int i=0;i<limbs;i+=2) {
    if(i!=0) {
      // integrate lo
      reset(carry,&globalParams);
      lo=add(lo, ulow(even[0]),&globalParams);
      carry=add(0, 0,&globalParams)!=0;
      even[0]=make_wide(lo, uhigh(even[0]));
    }

    reset(&globalParams);
    
    for(int j=0;j<limbs;j+=2)
      even[j/2]=madwide(a[i], b[j], even[j/2],&globalParams);
    c1=add(0, 0,&globalParams);

    reset(&globalParams);
    
    for(int j=0;j<limbs;j+=2)
      odd[j/2]=madwide(a[i], b[j+1], odd[j/2],&globalParams);

    // q=NP::qTerm(ulow(even[0]));
    q = zc - ulow(even[0]);
    reset(&globalParams);
    
    for(int j=0;j<limbs;j+=2)
      odd[j/2]=madwide(q, n[j+1], odd[j/2],&globalParams);

    // shift even by 64bits
    reset(&globalParams);
    even[0]=madwide(q, n[0], even[0],&globalParams);
    lo=uhigh(even[0]);
    
    for(int j=2;j<limbs;j+=2)
      even[j/2-1]=madwide(q, n[j], even[j/2],&globalParams);
    c1=add(c1, 0,&globalParams);
      
    // integrate lo
    reset(carry,&globalParams);
    lo=add(lo, ulow(odd[0]),&globalParams);
    carry=add(0, 0,&globalParams)!=0;
    odd[0]=make_wide(lo, uhigh(odd[0]));

    reset(&globalParams);
    
    for(int j=0;j<limbs;j+=2)
      odd[j/2]=madwide(a[i+1], b[j], odd[j/2],&globalParams);
    c2=add(0, 0,&globalParams);

    // q=NP::qTerm(ulow(odd[0]));
    q = zc - ulow(odd[0]);
    // shift odd by 64 bits
    reset(&globalParams);
    odd[0]=madwide(q, n[0], odd[0],&globalParams);
    lo=uhigh(odd[0]);
    
    for(int j=2;j<limbs;j+=2)
      odd[j/2-1]=madwide(q, n[j], odd[j/2],&globalParams);
    c2=add(c2, 0,&globalParams);

    odd[limbs/2-1]=make_wide(0, 0);
    even[limbs/2-1]=make_wide(c1, c2);
    
    reset(&globalParams);
    
    for(int j=0;j<limbs;j+=2)
      even[j/2]=madwide(a[i+1], b[j+1], even[j/2],&globalParams);

    reset(&globalParams);
    
    for(int j=0;j<limbs;j+=2)
      even[j/2]=madwide(q, n[j+1], even[j/2],&globalParams);
  }

  reset(carry,&globalParams);
  lo=add(lo, ulow(even[0]),&globalParams);
  carry=add(0, 0,&globalParams)!=0;
  even[0]=make_wide(lo, uhigh(even[0]));
#else
  
  for(int i=0;i<limbs/2;i++) {
    even[i]=make_wide(0, 0);
    odd[i]=make_wide(0, 0);
  }

  reset_(&globalParams);
    
  for(int j=0;j<limbs;j+=2)
      even[j/2]=madwide_(a[0], b[j], even[j/2],&globalParams);
  c1=add_(0, 0);

  reset_(&globalParams);
  
  for(int j=0;j<limbs;j+=2)
      odd[j/2]=madwide_(a[0], b[j+1], odd[j/2],&globalParams);

  q = zc - ulow(even[0]);
  reset_(&globalParams);
  
  for(int j=0;j<limbs;j+=2)
      odd[j/2]=madwide_(q, n[j+1], odd[j/2],&globalParams);

    // shift even by 64bits
  reset_(&globalParams);
  even[0]=madwide_(q, n[0], even[0],&globalParams);
  lo=uhigh(even[0]);
  
  for(int j=2;j<limbs;j+=2)
    even[j/2-1]=madwide_(q, n[j], even[j/2],&globalParams);
  c1=add_(c1, 0);
      
  // integrate lo
  reset(carry);
  lo=add(lo, ulow(odd[0]));
  carry=add(0, 0)!=0;
  odd[0]=make_wide(lo, uhigh(odd[0]));

  reset_(&globalParams);
  
  for(int j=0;j<limbs;j+=2)
      odd[j/2]=madwide_(a[1], b[j], odd[j/2],&globalParams);
  c2=add_(0, 0);

  // q=NP::qTerm(ulow(odd[0]));
  q = zc - ulow(odd[0]);
  // shift odd by 64 bits
  reset_(&globalParams);
  odd[0]=madwide_(q, n[0], odd[0],&globalParams);
  lo=uhigh(odd[0]);
  
  for(int j=2;j<limbs;j+=2)
      odd[j/2-1]=madwide_(q, n[j], odd[j/2],&globalParams);
  c2=add_(c2, 0);

  odd[limbs/2-1]=make_wide(0, 0);
  even[limbs/2-1]=make_wide(c1, c2);
    
  reset_(&globalParams);
  
  for(int j=0;j<limbs;j+=2)
    even[j/2]=madwide_(a[1], b[j+1], even[j/2],&globalParams);

  reset(&globalParams);
  
  for(int j=0;j<limbs;j+=2)
    even[j/2]=madwide_(q, n[j+1], even[j/2],&globalParams);


  
  for(int i=2;i<limbs;i+=2) {
    // integrate lo
    reset(carry,&globalParams);
    lo=add(lo, ulow(even[0]));
    carry=add(0, 0,&globalParams)!=0;
    even[0]=make_wide(lo, uhigh(even[0]));

    reset_(&globalParams);
    
    for(int j=0;j<limbs;j+=2)
      even[j/2]=madwide_(a[i], b[j], even[j/2],&globalParams);
    c1=add_(0, 0,&globalParams);

    reset_(&globalParams);
    
    for(int j=0;j<limbs;j+=2)
      odd[j/2]=madwide_(a[i], b[j+1], odd[j/2],&globalParams);

    // q=NP::qTerm(ulow(even[0]));
    q = zc - ulow(even[0]);
    reset_(&globalParams);
    
    for(int j=0;j<limbs;j+=2)
      odd[j/2]=madwide_(q, n[j+1], odd[j/2],&globalParams);

    // shift even by 64bits
    reset_(&globalParams);
    even[0]=madwide_(q, n[0], even[0],&globalParams);
    lo=uhigh(even[0]);
    
    for(int j=2;j<limbs;j+=2)
      even[j/2-1]=madwide_(q, n[j], even[j/2],&globalParams);
    c1=add_(c1, 0,&globalParams);
      
    // integrate lo
    reset(carry,&globalParams);
    lo=add(lo, ulow(odd[0]),&globalParams);
    carry=add(0, 0,&globalParams)!=0;
    odd[0]=make_wide(lo, uhigh(odd[0]));

    reset_(&globalParams);
    
    for(int j=0;j<limbs;j+=2)
      odd[j/2]=madwide_(a[i+1], b[j], odd[j/2],&globalParams);
    c2=add_(0, 0);

    // q=NP::qTerm(ulow(odd[0]));
    q = zc - ulow(odd[0]);
    // shift odd by 64 bits
    reset_(&globalParams);
    odd[0]=madwide_(q, n[0], odd[0],&globalParams);
    lo=uhigh(odd[0]);
    
    for(int j=2;j<limbs;j+=2)
      odd[j/2-1]=madwide_(q, n[j], odd[j/2],&globalParams);
    c2=add_(c2, 0);

    odd[limbs/2-1]=make_wide(0, 0);
    even[limbs/2-1]=make_wide(c1, c2);
    
    reset_(&globalParams);
    
    for(int j=0;j<limbs;j+=2)
      even[j/2]=madwide_(a[i+1], b[j+1], even[j/2],&globalParams);

    reset_(&globalParams);
    
    for(int j=0;j<limbs;j+=2)
      even[j/2]=madwide_(q, n[j+1], even[j/2],&globalParams);
  }

  reset(carry,&globalParams);
  lo=add(lo, ulow(even[0]));
  carry=add(0, 0,&globalParams)!=0;
  even[0]=make_wide(lo, uhigh(even[0]));

#endif
  return carry;
}


 bool mp_sqr_red_cl(int limbs,ulong* evenOdd, uint* temp, const uint* a, const uint* n) {
  ulong* even=evenOdd;
  ulong* odd=evenOdd + limbs/2;
  GlobalParameters globalParams;
  chain_t(&globalParams);
  bool      carry=false;
  uint  lo=0, q, c1, c2, low, high;
  
  // This routine can be used when a+n < R (i.e. it doesn't carry out).  Hence the name cl for carryless.
  // Only works with an even number of limbs.
  
  mp_zero(limbs,temp);
  
  
  for(int i=0;i<limbs/2;i++) {
    even[i]=make_wide(0, 0);
    odd[i]=make_wide(0, 0);
  }
  
  // do odds
  for(int j=limbs-1;j>0;j-=2) {
    reset(&globalParams);
    for(int i=0;i<limbs-j;i++)
      evenOdd[j/2+i+1]=madwide(a[i], a[i+j], evenOdd[j/2+i+1],&globalParams);
  }

  // shift right
  for(int i=0;i<limbs-1;i++)
    evenOdd[i]=make_wide(uhigh(evenOdd[i]), ulow(evenOdd[i+1]));
  evenOdd[limbs-1]=make_wide(uhigh(evenOdd[limbs-1]), 0);
   
  // do evens
  for(int j=limbs-2;j>0;j-=2) {
    reset(&globalParams);
    for(int i=0;i<limbs-j;i++) 
      evenOdd[j/2+i]=madwide(a[i], a[i+j], evenOdd[j/2+i],&globalParams);
    temp[limbs-j]=(add(0, 0,&globalParams)!=0) ? 2 : 0;
  }

  // double
  reset(&globalParams);
  for(int i=0;i<limbs;i++) {
    low=add(ulow(evenOdd[i]), ulow(evenOdd[i]),&globalParams);
    high=add(uhigh(evenOdd[i]), uhigh(evenOdd[i]),&globalParams);
    evenOdd[i]=make_wide(low, high);
  }

  // add diagonals
  reset(&globalParams);
  for(int i=0;i<limbs;i++) 
    evenOdd[i]=madwide(a[i], a[i], evenOdd[i],&globalParams);

  // add high part of wide to b...
  reset(&globalParams);
  for(int i=0;i<limbs;i+=2) {
    temp[i]=add(ulow(evenOdd[limbs/2+i/2]), temp[i],&globalParams);
    temp[i+1]=add(uhigh(evenOdd[limbs/2+i/2]), temp[i+1],&globalParams);
  }

  
  for(int i=0;i<limbs/2;i++) 
    odd[i]=make_wide(0, 0);

  // now we need to reduce
  
  for(int i=0;i<limbs/2;i++) {
    if(i!=0) {
      // integrate lo
      reset(carry,&globalParams);
      lo=add(lo, ulow(even[0]),&globalParams);
      carry=add(0, 0,&globalParams)!=0;
      even[0]=make_wide(lo, uhigh(even[0]));
    }
    
    // q=NP::qTerm(ulow(even[0]));
    q = zc - ulow(even[0]); 
    // shift even by 64 bits
    reset(&globalParams);
    even[0]=madwide(q, n[0], even[0],&globalParams);
    lo=uhigh(even[0]);
    
    for(int j=2;j<limbs;j+=2)
      even[j/2-1]=madwide(q, n[j], even[j/2],&globalParams);
    c1=add(0, 0,&globalParams);

    reset(&globalParams);
    
    for(int j=0;j<limbs;j+=2)
      odd[j/2]=madwide(q, n[j+1], odd[j/2],&globalParams);
      
    // second half

    // integrate lo
    reset(carry,&globalParams);
    lo=add(lo, ulow(odd[0]),&globalParams);
    carry=add(0, 0,&globalParams)!=0;
    odd[0]=make_wide(lo, uhigh(odd[0]));
    
    // q=NP::qTerm(ulow(odd[0]));
    q = zc - ulow(odd[0]);
    // shift odd by 64 bits
    reset(&globalParams);
    odd[0]=madwide(q, n[0], odd[0],&globalParams);
    lo=uhigh(odd[0]);
    for(int j=2;j<limbs;j+=2)
      odd[j/2-1]=madwide(q, n[j], odd[j/2],&globalParams);
    odd[limbs/2-1]=0;
    c2=add(0, 0,&globalParams);

    reset(&globalParams);
    for(int j=0;j<limbs-2;j+=2)
      even[j/2]=madwide(q, n[j+1], even[j/2],&globalParams);
    even[limbs/2-1]=madwide(q, n[limbs-1], make_wide(c1, c2),&globalParams);
  }
  
  reset(&globalParams);
  for(int i=0;i<limbs;i+=2) {
    low=add(ulow(even[i/2]), temp[i],&globalParams);
    high=add(uhigh(even[i/2]), temp[i+1],&globalParams);
    even[i/2]=make_wide(low, high);
  }
  
  reset(carry,&globalParams);
  lo=add(lo, ulow(even[0]),&globalParams);
  carry=add(0, 0,&globalParams)!=0;
  even[0]=make_wide(lo, uhigh(even[0]));
  return carry;
}


 void mp_merge_cl(int limbs,uint* r, const ulong* evenOdd, bool carry) {
  GlobalParameters globalParams;
  chain_t(carry,&globalParams);
 
  r[0]=ulow(evenOdd[0]);
  for(int i=0;i<limbs/2-1;i++) {
    r[2*i+1]=add(uhigh(evenOdd[i]), ulow(evenOdd[limbs/2 + i]),&globalParams);
    r[2*i+2]=add(ulow(evenOdd[i+1]), uhigh(evenOdd[limbs/2 + i]),&globalParams);
  }
  r[limbs-1]=add(uhigh(evenOdd[limbs/2-1]), 0,&globalParams);
}



/** MP.cu end*/


/** big_base_compute.cu start*/
void sub_assign_big_384(uint *var_l, uint *var_r,
                                                    uint *dst, uint *temp,
                                                    uint *N) {
    if (mp_comp_gt(LEN,var_r, var_l)) {
        mp_add(LEN,temp, N, var_l);
        mp_sub(LEN,dst, temp, var_r);
    } else {
       mp_sub(LEN,dst, var_l, var_r);
    }
}

void sub_assign_big_256(uint *var_l, uint *var_r,
                                                    uint *dst, uint *temp,
                                                    uint *N) {
    if (mp_comp_gt(8,var_r, var_l)) {
        mp_add(8,temp, N, var_l);
        mp_sub(8,dst, temp, var_r);
    } else {
       mp_sub(8,dst, var_l, var_r);
    }
}

void add_assign_big_384(uint *var_l, uint *var_r,
                                                    uint *dst, uint *N) {
    mp_add(LEN,dst, var_l, var_r);
    if (mp_comp_ge(LEN,dst, N)){
      mp_sub(LEN,dst, dst, N);
   }
}

void add_assign_big_256(uint *var_l, uint *var_r,
                                                    uint *dst, uint *N) {
    mp_add(8,dst, var_l, var_r);
    if (mp_comp_ge(8,dst, N)){
      mp_sub(8,dst, dst, N);
   }
}

void mul_assign_big_384(uint *var_l, uint *var_r,
                                                   uint *dst, uint *N) {
   bool carry;
   ulong evenodd[12];

   carry = mp_mul_red_cl(LEN,evenodd, var_l, var_r, N);
   mp_merge_cl(LEN,dst, evenodd, carry); 
   if (mp_comp_ge(LEN,dst, N)){
      mp_sub(LEN,dst, dst, N);
   }
}

void mul_assign_big_256(uint *var_l, uint *var_r,
                                                   uint *dst, uint *N) {
   bool carry;
   ulong evenodd[8];

   carry = mp_mul_red_cl(8,evenodd, var_l, var_r, N);
   mp_merge_cl(8,dst, evenodd, carry); 
   if (mp_comp_ge(8,dst, N)){
      mp_sub(8,dst, dst, N);
   }
}

void sub_big_384(uint *var_l, uint *var_r,
                                             uint *dst) {
    mp_sub(LEN,dst, var_l, var_r);
}

void add_big_384(uint *var_l, uint *var_r,
                                             uint *dst) {
    mp_add(LEN,dst, var_l, var_r);
}

void mul_big_384(uint *var_l, uint *var_r,
                                             uint *dst, uint *N) {
   bool carry;
   ulong evenodd[12];

   carry = mp_mul_red_cl(LEN,evenodd, var_l, var_r, N);
   mp_merge_cl(LEN,dst, evenodd, carry); 
}

void sqr_big_384(uint *temp, uint *var_l,
                                             uint *dst, uint *N) {
   bool carry;
   ulong evenodd[12];
   carry = mp_sqr_red_cl(LEN,evenodd, temp, var_l, N);
   mp_merge_cl(LEN,dst, evenodd, carry); 
}

void sqr_assign_big_384(uint *temp, uint *var_l,
                                                    uint *dst, uint *N) {
   bool carry;
   ulong evenodd[12];
   carry = mp_sqr_red_cl(LEN,evenodd, temp, var_l, N);
   mp_merge_cl(LEN,dst, evenodd, carry);
   if (mp_comp_ge(LEN,dst, N)) {
      mp_sub(LEN,dst, dst, N);
   }
}

void double_big_384(uint *var_l, uint *var_r) {
    mp_shift_left(LEN,var_r, var_l, (uint)1);
}

void double_assign_big_384(uint *var_l, uint *var_r, uint *N) {
   mp_shift_left(LEN,var_r, var_l, (uint)1);
   if (mp_comp_ge(LEN,var_r, N)) {
      mp_sub(LEN,var_r, var_r, N);
   }
}

int cmp_big_384(uint *var_l, uint *var_r) {
#if 1   
   if(mp_comp_gt(LEN,var_l, var_r)) {
      return 1;
   } else if (mp_comp_eq(LEN,var_l, var_r)) {
      return 0;
   }   else {
      return -1;
   }
#else
  return mp_comp_gt(LEN,var_l, var_r);
#endif
}

void zeros_big_384(uint *var_l) {
    mp_zero(LEN,var_l);
}

void cpy_big_384(uint *dst, uint *var_l) {
    mp_copy(LEN,dst, var_l);
}

uint __umulhi(uint a,uint b) {
  ulong r = (ulong)a * (ulong)b;
  return uhigh(r);
}

uint4 load_shared_u4(uint x,__local uint *temp) {
  uint4 r;
  uint start = x/4;
  
  r[0] = temp[start];
  r[1] = temp[start+1];
  r[2] = temp[start+2];
  r[3] = temp[start+3];
  return r;
}

uint __funnelshift_l( uint  lo, uint  hi, uint  shift ) {
  ulong r = make_wide(lo,hi);
  r = r << (shift & 31);
  return uhigh(r);
}

 bool is_zero_big_384_xyzz(uint *var_l,
                                                     uint *N,__local uint *temp) {
    uint x, mult;
    uint4 r;
    uint i = get_local_id(0)&0x1F;
    // note, this routine only works for  <= 8N
    // __local uint *temp;
    x=__funnelshift_l(var_l[10], var_l[11], 4);
    mult=__umulhi(10, x);
    mult = mult * 0x1AE3A461;
    mult = mult + 3;
    mult = mult - x;
    // __local uint temp[]
    if(mult<=3u) { // temp[0+mult*48+i*128*4] 192*32*4 ModuleTable 192*4
    // asm volatile ("ld.shared.v4.u32 {%0,%1,%2,%3},[%4];" : "=r"(r.x), "=r"(r.y), "=r"(r.z), "=r"(r.w) : "r"(sAddr));
    //start = 0+mult*48+i*128*4
    // temp[start/4]
    // temp[start/4+4]
    // temp[start/4+8]
      r = load_shared_u4(0+mult*48+i*128*4,temp) ;
      N[0] = r.x;
      N[1] = r.y;
      N[2] = r.z;
      N[3] = r.w;
      r = load_shared_u4(16+mult*48+i*128*4,temp);
      N[4] = r.x;
      N[5] = r.y;
      N[6] = r.z;
      N[7] = r.w;
      r = load_shared_u4(2*16+mult*48+i*128*4,temp);
      N[8] = r.x;
      N[9] = r.y;
      N[10] = r.z;
      N[11] = r.w;
      return mp_comp_eq(LEN,N, var_l);
    }
    return false;
}

 bool is_zero_big_384_xyzz_(uint *var_l,
                                                      uint *N,__local uint *temp) {
    uint x, mult;
    uint4 r;
    uint i = get_local_id(0)&0x1F;
    
    // note, this routine only works for  <= 8N
    x=__funnelshift_l(var_l[10], var_l[11], 4);
    mult=__umulhi(10, x);
    // if(i==1){
    //         printf("is_zero_big_384_xyzz_=================%u %u x:%u mult:%u",var_l[10],var_l[11],x,mult);
    //         return false;
    //     }
    mult = mult * 0x1AE3A461;
    mult = mult + 3;
    mult = mult - x;
    if(mult<=3u) {
        
      r = load_shared_u4(0+mult*48+i*192*4,temp) ;
      N[0] = r.x;
      N[1] = r.y;
      N[2] = r.z;
      N[3] = r.w;
      r = load_shared_u4(16+mult*48+i*192*4,temp);
      N[4] = r.x;
      N[5] = r.y;
      N[6] = r.z;
      N[7] = r.w;
      r = load_shared_u4(2*16+mult*48+i*192*4,temp);
      N[8] = r.x;
      N[9] = r.y;
      N[10] = r.z;
      N[11] = r.w;
      return mp_comp_eq(LEN,N, var_l);
    }
    return false;
}

 bool is_zero_big_384_xyzz_f(uint *var_l) {
    uint x, mult;
/*    
    uint N[48] = {0x00000000, 0x00000000, 0x00000000, 0x00000000,
                      0x00000000, 0x00000000, 0x00000000, 0x00000000,
                      0x00000000, 0x00000000, 0x00000000, 0x00000000,
                      0x00000001, 0x8508C000, 0x30000000, 0x170B5D44,
                      0xBA094800, 0x1EF3622F, 0x00F5138F, 0x1A22D9F3,
                      0x6CA1493B, 0xC63B05C0, 0x17C510EA, 0x01AE3A46,
                      0x00000002, 0x0A118000, 0x60000001, 0x2E16BA88,
                      0x74129000, 0x3DE6C45F, 0x01EA271E, 0x3445B3E6,
                      0xD9429276, 0x8C760B80, 0x2F8A21D5, 0x035C748C,
                      0x00000003, 0x8F1A4000, 0x90000001, 0x452217CC,
                      0x2E1BD800, 0x5CDA268F, 0x02DF3AAD, 0x4E688DD9,
                      0x45E3DBB1, 0x52B11141, 0x474F32C0, 0x050AAED2};
*/  
    uint N0[12] = {0x00000000, 0x00000000, 0x00000000, 0x00000000,
                      0x00000000, 0x00000000, 0x00000000, 0x00000000,
                      0x00000000, 0x00000000, 0x00000000, 0x00000000};
    uint N1[12] = {0x00000001, 0x8508C000, 0x30000000, 0x170B5D44,
                      0xBA094800, 0x1EF3622F, 0x00F5138F, 0x1A22D9F3,
                      0x6CA1493B, 0xC63B05C0, 0x17C510EA, 0x01AE3A46};
    uint N2[12] = {0x00000002, 0x0A118000, 0x60000001, 0x2E16BA88,
                      0x74129000, 0x3DE6C45F, 0x01EA271E, 0x3445B3E6,
                      0xD9429276, 0x8C760B80, 0x2F8A21D5, 0x035C748C};
    uint N3[12] = {0x00000003, 0x8F1A4000, 0x90000001, 0x452217CC,
                      0x2E1BD800, 0x5CDA268F, 0x02DF3AAD, 0x4E688DD9,
                      0x45E3DBB1, 0x52B11141, 0x474F32C0, 0x050AAED2};                                                                 
    // note, this routine only works for  <= 8N
    x=__funnelshift_l(var_l[10], var_l[11], 4);
    mult=__umulhi(10, x);
    mult = mult * 0x1AE3A461;
    mult = mult + 3;
    mult = mult - x;
/*    
    if(mult<=3u) {
      return mp_comp_eq(LEN,&N[mult*12], var_l);
    }
*/
    switch(mult) {
      case 0:
        return mp_comp_eq(LEN,N0, var_l);
      break;
      case 1:
        return mp_comp_eq(LEN,N1, var_l); 
      break;
      case 2:
        return mp_comp_eq(LEN,N2, var_l);
      break;
      case 3:
        return mp_comp_eq(LEN,N3, var_l); 
      break;
    }   
    return false;
}

 bool is_zero_big_384(uint* var){
  uint temp; 
  temp = var[0];
  #pragma unroll
  for (int i=1;i<12; i++) {
     temp |= var[i];   
  }
#if 0  
  if (temp == 0) {
     return 1; 
  } else {
     return 0; 
  }
#endif
  return (temp == 0);
}

 bool is_zero_big_256(uint* var){
  uint temp; 
  temp = var[0];
  #pragma unroll
  for (int i=1;i<8; i++) {
     temp |= var[i];   
  }
#if 0
  if (temp == 0) {
     return 1; 
  } else {
     return 0; 
  }
#endif
  return (temp == 0);  
}


 bool is_one_big_384(uint* var){
  uint R_384[12]; 
  R_384[0] = 0xffffff68, R_384[1] = 0x02cdffff, R_384[2] = 0x7fffffb1;
  R_384[3] = 0x51409f83, R_384[4] = 0x8a7d3ff2, R_384[5] = 0x9f7db3a9;
  R_384[6] = 0x6e7c6305, R_384[7] = 0x7b4e97b7, R_384[8] = 0x803c84e8;
  R_384[9] = 0x4cf495bf, R_384[10] = 0xe2fdf49a, R_384[11] = 0x008d6661;
  if(mp_comp_eq(LEN,var, R_384)){
    return 1; 
  } else {
    return 0;
  }
}

 void inverse_big_384(uint* r, uint* var, uint *N_) {
  bool temp;
  uint R2_384[12];   //b
  uint ZERO_384[12]; //c
  uint N[12];
  uint one[12];
  uint temp_[12];
  uint temp1[12];
  ZERO_384[0] = 0x00000000, ZERO_384[1]=0x00000000, ZERO_384[2]=0x00000000;
  ZERO_384[3] = 0x00000000, ZERO_384[4]=0x00000000, ZERO_384[5]=0x00000000;
  ZERO_384[6] = 0x00000000, ZERO_384[7]=0x00000000, ZERO_384[8]=0x00000000;
  ZERO_384[9] = 0x00000000, ZERO_384[10]=0x00000000, ZERO_384[11]=0x00000000;
  one[0] = 0x00000001, one[1] = 0x00000000, one[2] = 0x00000000;
  one[3] = 0x00000000, one[4] = 0x00000000, one[5] = 0x00000000;
  one[6] = 0x00000000, one[7] = 0x00000000, one[8] = 0x00000000;
  one[9] = 0x00000000, one[10] = 0x00000000, one[11] = 0x00000000;
  N[0]= 0x00000001, N[1]= 0x8508C000, N[2]= 0x30000000, N[3]= 0x170B5D44;
  N[4]= 0xBA094800, N[5]= 0x1EF3622F, N[6]= 0x00F5138F, N[7]= 0x1A22D9F3;
  N[8]= 0x6CA1493B, N[9]= 0xC63B05C0, N[10]= 0x17C510EA,N[11]= 0x01AE3A46;
  R2_384[0] = 0x9400cd22, R2_384[1] =0xb786686c, R2_384[2] = 0xb00431b1;
  R2_384[3] = 0x0329fcaa, R2_384[4] =0x62d6b46d, R2_384[5] = 0x22a5f111 ;
  R2_384[6] = 0x827dc3ac, R2_384[7] =0xbfdf7d03, R2_384[8] = 0x41790bf9;
  R2_384[9] = 0x837e92f0, R2_384[10] =0x1e914b88, R2_384[11] =0x006dfccb;

  if (is_zero_big_384(var)) {
     mp_copy(LEN,r, var);
     return;
  }
  mp_copy(LEN,temp1, var);

  while ( !mp_comp_eq(LEN,temp1, one) &&
          !mp_comp_eq(LEN,N, one)) {      
        while(!(temp1[0]&0x00000001)) {
           mp_shift_right(LEN,temp1, temp1, 1); 
           if (!(R2_384[0]&0x00000001)) {
              mp_shift_right(LEN,R2_384, R2_384, 1);
           } else {
              mp_add(LEN,R2_384, R2_384, N_);
              mp_shift_right(LEN,R2_384, R2_384, 1);
           }  
        }
        while(!(N[0]&0x00000001)) {
           mp_shift_right(LEN,N, N, 1); 
           if (!(ZERO_384[0]&0x00000001)) {
               mp_shift_right(LEN,ZERO_384, ZERO_384, 1);
           } else {
               mp_add(LEN,ZERO_384, ZERO_384, N_);
               mp_shift_right(LEN,ZERO_384, ZERO_384, 1);
           }
        }
        if(mp_comp_gt(LEN,temp1, N)) {
            mp_sub(LEN,temp1, temp1, N);
            sub_assign_big_384(R2_384,ZERO_384,R2_384, temp_, N_);
        } else {
            mp_sub(LEN,N, N, temp1);
            sub_assign_big_384(ZERO_384,R2_384,ZERO_384, temp_, N_);
        }
  }
  if(mp_comp_eq(LEN,temp1, one)) {
     mp_copy(LEN,r, R2_384);  
  } else {
     mp_copy(LEN,r, ZERO_384); 
  }
}

void  xyzzReduce(uint* r, uint* var, uint* N,__local uint* temp) {
    uint x, mult;
    uint4 r_;
    uint i = get_local_id(0)&0x1F;
    uint offset;
    int carry;

    x=__funnelshift_l(var[10], var[11], 4);
    mult=__umulhi(10, x);

    r_ = load_shared_u4(0 + mult*48 + i*128*4,temp);
    N[0] = r_.x;
    N[1] = r_.y;
    N[2] = r_.z;
    N[3] = r_.w;
    r_ = load_shared_u4(16 + mult*48 +i*128*4,temp);
    N[4] = r_.x;
    N[5] = r_.y;
    N[6] = r_.z;
    N[7] = r_.w;
    r_ = load_shared_u4(2*16 + mult*48 +i*128*4,temp);
    N[8] = r_.x;
    N[9] = r_.y;
    N[10] = r_.z;
    N[11] = r_.w;

    if(!mp_sub_carry(LEN,r, var, N)) {
      r_ = load_shared_u4(0+48+i*128*4,temp);
      N[0] = r_.x;
      N[1] = r_.y;
      N[2] = r_.z;
      N[3] = r_.w;
      r_ = load_shared_u4(16+48+i*128*4,temp);
      N[4] = r_.x;
      N[5] = r_.y;
      N[6] = r_.z;
      N[7] = r_.w;
      r_ = load_shared_u4(2*16+48+i*128*4,temp);
      N[8] = r_.x;
      N[9] = r_.y;
      N[10] = r_.z;
      N[11] = r_.w;      
      mp_add(LEN,r, r, N);
    }
}

void xyzzReduce_(uint* r, uint* var, uint* N,__local uint *temp) {
    uint x, mult;
    uint4 r_;
    uint i = get_local_id(0)&0x1F;
    uint offset;
    int carry;

    x=__funnelshift_l(var[10], var[11], 4);
    mult=__umulhi(10, x);

    r_ = load_shared_u4(0 + mult*48 + i*192*4,temp);
    N[0] = r_.x;
    N[1] = r_.y;
    N[2] = r_.z;
    N[3] = r_.w;
    r_ = load_shared_u4(16 + mult*48 +i*192*4,temp);
    N[4] = r_.x;
    N[5] = r_.y;
    N[6] = r_.z;
    N[7] = r_.w;
    r_ = load_shared_u4(2*16 + mult*48 +i*192*4,temp);
    N[8] = r_.x;
    N[9] = r_.y;
    N[10] = r_.z;
    N[11] = r_.w;

    if(!mp_sub_carry(LEN,r, var, N)) {
      r_ = load_shared_u4(0+48+i*192*4,temp);
      N[0] = r_.x;
      N[1] = r_.y;
      N[2] = r_.z;
      N[3] = r_.w;
      r_ = load_shared_u4(16+48+i*192*4,temp);
      N[4] = r_.x;
      N[5] = r_.y;
      N[6] = r_.z;
      N[7] = r_.w;
      r_ = load_shared_u4(2*16+48+i*192*4,temp);
      N[8] = r_.x;
      N[9] = r_.y;
      N[10] = r_.z;
      N[11] = r_.w;      
      mp_add(LEN,r, r, N);
    }
}


 ulong big_make_wide(uint lo, uint hi) {
  return make_wide(lo, hi);
}

 uint big_ulow(ulong var) {
  return ulow(var);
}

 uint big_uhigh(ulong var) {
  return uhigh(var);
}

 ulong mac_carry(ulong a, ulong b, ulong c, ulong *carry) {
    __uint128_t result;
    result = (__uint128_t)b * c + a + *carry;
    ulong res_low = result;
    ulong res_high = (result >> 64);
    *carry = res_high;
    return res_low;
}
void big_from_repr_256(uint* var, uint* r) {
  
  uint R2[8];
  uint N[8];
  N[0] = 0x00000001, N[1] = 0xa118000;
  N[2] = 0xd0000001, N[3] = 0x59aa76fe;
  N[4] = 0x5c37b001, N[5] = 0x60b44d1e;
  N[6] = 0x9a2ca556, N[7] = 0x12ab655e;
  R2[0] = 0xb861857b, R2[1] =0x25d577ba; 
  R2[2] = 0x8860591f, R2[3] =0xcc2c27b5;
  R2[4] = 0xe5dc8593, R2[5] = 0xa7cc008f;
  R2[6] = 0xeff1c939, R2[7] =  0x11fdae7;
  mul_assign_big_256(var, R2, r, N);
}

void big_to_repr_256(ulong* r) {   
    ulong k;
    ulong INV = 725501752471715839;
    ulong N[4];
    N[0] = 0xa11800000000001;
    N[1] = 0x59aa76fed0000001;
    N[2] = 0x60b44d1e5c37b001;
    N[3] = 0x12ab655e9a2ca556;
    k = r[0] * INV;
    ulong carry = 0;
    mac_carry(r[0], k, N[0], &carry);
    r[1] = mac_carry(r[1], k, N[1], &carry);
    r[2] = mac_carry(r[2], k, N[2], &carry);
    r[3] = mac_carry(r[3], k, N[3], &carry);
    r[0] = carry;

    k = r[1] * INV;
    carry = 0;
    mac_carry(r[1], k, N[0], &carry);
    r[2] = mac_carry(r[2], k, N[1], &carry);
    r[3] = mac_carry(r[3], k, N[2], &carry);
    r[0] = mac_carry(r[0], k, N[3], &carry);
    r[1] = carry;

    k = r[2] * INV;
    carry = 0;
    mac_carry(r[2], k, N[0], &carry);
    r[3] = mac_carry(r[3], k, N[1], &carry);
    r[0] = mac_carry(r[0], k, N[2], &carry);
    r[1] = mac_carry(r[1], k, N[3], &carry);
    r[2] = carry;

    k = r[3] * INV;
    carry = 0;
    mac_carry(r[3], k, N[0], &carry);
    r[0] = mac_carry(r[0], k, N[1], &carry);
    r[1] = mac_carry(r[1], k, N[2], &carry);
    r[2] = mac_carry(r[2], k, N[3], &carry);
    r[3] = carry;
}

void __attribute__((overloadable)) big_to_repr_384(uint *var)
{
    ulong INV_384 = 9586122913090633727ull;
    // Montgomery Reduction
    // Iteration 0
    ulong k = (ulong)0;
    ulong N[6];
    ulong r[6];
    N[0] = 0x8508c00000000001;
    N[1] = 0x170b5d4430000000;
    N[2] = 0x1ef3622fba094800;
    N[3] = 0x1a22d9f300f5138f;
    N[4] = 0xc63b05c06ca1493b;
    N[5] = 0x1ae3a4617c510ea;
    r[0] = big_make_wide(var[0], var[1]);
    r[1] = big_make_wide(var[2], var[3]);
    r[2] = big_make_wide(var[4], var[5]);
    r[3] = big_make_wide(var[6], var[7]);
    r[4] = big_make_wide(var[8], var[9]);
    r[5] = big_make_wide(var[10], var[11]);

    k = r[0] * INV_384;
    ulong carry = 0;
    mac_carry(r[0], k, N[0], &carry);
    r[1] = mac_carry(r[1], k, N[1], &carry);
    r[2] = mac_carry(r[2], k, N[2], &carry);
    r[3] = mac_carry(r[3], k, N[3], &carry);
    r[4] = mac_carry(r[4], k, N[4], &carry);
    r[5] = mac_carry(r[5], k, N[5], &carry);
    r[0] = carry;

    k = r[1] * INV_384;
    carry = 0;
    mac_carry(r[1], k, N[0], &carry);
    r[2] = mac_carry(r[2], k, N[1], &carry);
    r[3] = mac_carry(r[3], k, N[2], &carry);
    r[4] = mac_carry(r[4], k, N[3], &carry);
    r[5] = mac_carry(r[5], k, N[4], &carry);
    r[0] = mac_carry(r[0], k, N[5], &carry);
    r[1] = carry;

    k = r[2] * INV_384;
    carry = 0;
    mac_carry(r[2], k, N[0], &carry);
    r[3] = mac_carry(r[3], k, N[1], &carry);
    r[4] = mac_carry(r[4], k, N[2], &carry);
    r[5] = mac_carry(r[5], k, N[3], &carry);
    r[0] = mac_carry(r[0], k, N[4], &carry);
    r[1] = mac_carry(r[1], k, N[5], &carry);
    r[2] = carry;

    k = r[3] * INV_384;
    carry = 0;
    mac_carry(r[3], k, N[0], &carry);
    r[4] = mac_carry(r[4], k, N[1], &carry);
    r[5] = mac_carry(r[5], k, N[2], &carry);
    r[0] = mac_carry(r[0], k, N[3], &carry);
    r[1] = mac_carry(r[1], k, N[4], &carry);
    r[2] = mac_carry(r[2], k, N[5], &carry);
    r[3] = carry;

    k = r[4] * INV_384;
    carry = 0;
    mac_carry(r[4], k, N[0], &carry);
    r[5] = mac_carry(r[5], k, N[1], &carry);
    r[0] = mac_carry(r[0], k, N[2], &carry);
    r[1] = mac_carry(r[1], k, N[3], &carry);
    r[2] = mac_carry(r[2], k, N[4], &carry);
    r[3] = mac_carry(r[3], k, N[5], &carry);
    r[4] = carry;

    k = r[5] * INV_384;
    carry = 0;
    mac_carry(r[5], k, N[0], &carry);
    r[0] = mac_carry(r[0], k, N[1], &carry);
    r[1] = mac_carry(r[1], k, N[2], &carry);
    r[2] = mac_carry(r[2], k, N[3], &carry);
    r[3] = mac_carry(r[3], k, N[4], &carry);
    r[4] = mac_carry(r[4], k, N[5], &carry);
    r[5] = carry;
}

void __attribute__((overloadable)) big_to_repr_384(uint *result, uint *var)
{
    ulong INV_384 = 9586122913090633727ull;
    // Montgomery Reduction
    // Iteration 0
    ulong k = (ulong)0;
    ulong N[6];
    ulong r[6];
    N[0] = 0x8508c00000000001;
    N[1] = 0x170b5d4430000000;
    N[2] = 0x1ef3622fba094800;
    N[3] = 0x1a22d9f300f5138f;
    N[4] = 0xc63b05c06ca1493b;
    N[5] = 0x1ae3a4617c510ea;
    r[0] = big_make_wide(var[0], var[1]);
    r[1] = big_make_wide(var[2], var[3]);
    r[2] = big_make_wide(var[4], var[5]);
    r[3] = big_make_wide(var[6], var[7]);
    r[4] = big_make_wide(var[8], var[9]);
    r[5] = big_make_wide(var[10], var[11]);

    k = r[0] * INV_384;
    ulong carry = 0;
    mac_carry(r[0], k, N[0], &carry);
    r[1] = mac_carry(r[1], k, N[1], &carry);
    r[2] = mac_carry(r[2], k, N[2], &carry);
    r[3] = mac_carry(r[3], k, N[3], &carry);
    r[4] = mac_carry(r[4], k, N[4], &carry);
    r[5] = mac_carry(r[5], k, N[5], &carry);
    r[0] = carry;

    k = r[1] * INV_384;
    carry = 0;
    mac_carry(r[1], k, N[0], &carry);
    r[2] = mac_carry(r[2], k, N[1], &carry);
    r[3] = mac_carry(r[3], k, N[2], &carry);
    r[4] = mac_carry(r[4], k, N[3], &carry);
    r[5] = mac_carry(r[5], k, N[4], &carry);
    r[0] = mac_carry(r[0], k, N[5], &carry);
    r[1] = carry;

    k = r[2] * INV_384;
    carry = 0;
    mac_carry(r[2], k, N[0], &carry);
    r[3] = mac_carry(r[3], k, N[1], &carry);
    r[4] = mac_carry(r[4], k, N[2], &carry);
    r[5] = mac_carry(r[5], k, N[3], &carry);
    r[0] = mac_carry(r[0], k, N[4], &carry);
    r[1] = mac_carry(r[1], k, N[5], &carry);
    r[2] = carry;

    k = r[3] * INV_384;
    carry = 0;
    mac_carry(r[3], k, N[0], &carry);
    r[4] = mac_carry(r[4], k, N[1], &carry);
    r[5] = mac_carry(r[5], k, N[2], &carry);
    r[0] = mac_carry(r[0], k, N[3], &carry);
    r[1] = mac_carry(r[1], k, N[4], &carry);
    r[2] = mac_carry(r[2], k, N[5], &carry);
    r[3] = carry;

    k = r[4] * INV_384;
    carry = 0;
    mac_carry(r[4], k, N[0], &carry);
    r[5] = mac_carry(r[5], k, N[1], &carry);
    r[0] = mac_carry(r[0], k, N[2], &carry);
    r[1] = mac_carry(r[1], k, N[3], &carry);
    r[2] = mac_carry(r[2], k, N[4], &carry);
    r[3] = mac_carry(r[3], k, N[5], &carry);
    r[4] = carry;

    k = r[5] * INV_384;
    carry = 0;
    mac_carry(r[5], k, N[0], &carry);
    r[0] = mac_carry(r[0], k, N[1], &carry);
    r[1] = mac_carry(r[1], k, N[2], &carry);
    r[2] = mac_carry(r[2], k, N[3], &carry);
    r[3] = mac_carry(r[3], k, N[4], &carry);
    r[4] = mac_carry(r[4], k, N[5], &carry);
    r[5] = carry;

    result[0] = big_ulow(r[0]);
    result[1] = big_uhigh(r[0]);
    result[2] = big_ulow(r[1]);
    result[3] = big_uhigh(r[1]);
    result[4] = big_ulow(r[2]);
    result[5] = big_uhigh(r[2]);
    result[6] = big_ulow(r[3]);
    result[7] = big_uhigh(r[3]);
    result[8] = big_ulow(r[4]);
    result[9] = big_uhigh(r[4]);
    result[10] = big_ulow(r[5]);
    result[11] = big_uhigh(r[5]);
}

int projective384_Iszero(projective384 *a)
{
    int ret = 1;
    ret &= is_zero_big_384(a->x);
    ret &= is_one_big_384(a->y);
    ret &= is_zero_big_384(a->z);
    return ret;
}

int projective384xyzz_Iszero(projective384xyzz *a)
{
    int ret = 1;
    ret &= is_zero_big_384(a->zz);
    return ret;
}

void neg_big_384(uint*self,uint *result)
{  
    uint N[12];
    N[0]= 0x00000001, N[1]= 0x8508C000, N[2]= 0x30000000;
    N[3]= 0x170B5D44, N[4]= 0xBA094800, N[5]= 0x1EF3622F;
    N[6]= 0x00F5138F, N[7]= 0x1A22D9F3, N[8]= 0x6CA1493B;
    N[9]= 0xC63B05C0, N[10]= 0x17C510EA, N[11]= 0x01AE3A46;
    if (!is_zero_big_384(self)) {
        sub_big_384(N, self, result);
    }
    else
        *result = *self;
}

 int affine384_Iszero(affine384 *a)
{
    int ret = 1;
    ret &= is_zero_big_384(a->x);
    ret &= is_one_big_384(a->y);
    return ret;
}

/** big_base_compute.cu end*/

/** swj_compute.cu start */

 int get_bit_256(ulong *self, int i)
{
    if (i >= 64 * 4 || i < 0) {
        return 0;
    }
    int array = i / 64;
    int offset = i - array * 64;
    ulong ith_bit = 1;
    ith_bit <<= offset;
    if ((self[array] & ith_bit)) {
        return 1;
    }
    return 0;
}

 void projective384_neg(projective384 *var_l, projective384 *var_r) {
    var_l = var_r;
    if (!projective384_Iszero(var_r)) {
       neg_big_384(var_l->y, var_l->y);
    }
}

 void projective384_negxyzz(projective384xyzz *dst, projective384xyzz *src)
{
    *dst = *src;
    if (!projective384xyzz_Iszero(src))
        neg_big_384(dst->y, dst->y);
}

 void swj_double_in_place_xyzz(projective384xyzz *self) {
    uint U[12],V[12],W[12],S[12],M[12],temp_[12],temp1[12],temp[12]; //temp2[12];
    uint N[12];
    N[0]= 0x00000001, N[1]= 0x8508C000, N[2]= 0x30000000, N[3]= 0x170B5D44;
    N[4]= 0xBA094800, N[5]= 0x1EF3622F, N[6]= 0x00F5138F, N[7]= 0x1A22D9F3;
    N[8]= 0x6CA1493B, N[9]= 0xC63B05C0, N[10]= 0x17C510EA,N[11]= 0x01AE3A46;
    if (projective384xyzz_Iszero(self))
        return;

    double_assign_big_384(self->y, U, N);
    sqr_assign_big_384(temp_, U, V, N);
    mul_assign_big_384(U, V, W, N);
    mul_assign_big_384(self->x, V, S, N);
    sqr_assign_big_384(temp_, self->x, temp, N);
    double_assign_big_384(temp, M, N);
    add_assign_big_384(M, temp, M, N);

    sqr_assign_big_384(temp_, M, self->x, N);
    sub_assign_big_384(self->x, S, self->x, temp_, N);
    sub_assign_big_384(self->x, S, self->x, temp_, N);

    sub_assign_big_384(S, self->x, temp, temp_, N);
    mul_assign_big_384(temp, M, temp, N);

    mul_assign_big_384(W, self->y, temp1, N);
    sub_assign_big_384(temp, temp1, self->y, temp_, N);
    mul_assign_big_384(V, self->zz, self->zz, N);
    mul_assign_big_384(W, self->zzz, self->zzz, N);
}

 void swj_add_assign_xyzz(projective384xyzz *self, projective384xyzz *other,__local uint *sharedTemp) {
    uint dbl=0;
    uint A[12], B[12];
    uint T0[12], T1[12],  N[12], N2[12], N3[12], N4[12], N6[12];
    uint temp_[12], temp[12], temp_t[12];
    N[0]= 0x00000001, N[1]= 0x8508C000, N[2]= 0x30000000, N[3]= 0x170B5D44;
    N[4]= 0xBA094800, N[5]= 0x1EF3622F, N[6]= 0x00F5138F, N[7]= 0x1A22D9F3;
    N[8]= 0x6CA1493B, N[9]= 0xC63B05C0, N[10]= 0x17C510EA,N[11]= 0x01AE3A46;

    N2[0] = 0x00000002, N2[1] = 0x0A118000, N2[2]=0x60000001, N2[3] =0x2E16BA88;
    N2[4] = 0x74129000, N2[5] = 0x3DE6C45F, N2[6]=0x01EA271E, N2[7] =0x3445B3E6;
    N2[8] = 0xD9429276, N2[9] = 0x8C760B80, N2[10]=0x2F8A21D5, N2[11]=0x035C748C;

    N3[0] = 0x00000003, N3[1] = 0x8F1A4000, N3[2]=0x90000001, N3[3] =0x452217CC;
    N3[4] = 0x2E1BD800, N3[5] = 0x5CDA268F, N3[6]=0x02DF3AAD, N3[7] =0x4E688DD9;
    N3[8] = 0x45E3DBB1, N3[9] = 0x52B11141, N3[10]=0x474F32C0, N3[11]=0x050AAED2;

    N4[0] = 0x00000004, N4[1] = 0x14230000, N4[2]=0xC0000002, N4[3] =0x5C2D7510;
    N4[4] = 0xE8252000, N4[5] = 0x7BCD88BE, N4[6]=0x03D44E3C, N4[7] =0x688B67CC;
    N4[8] = 0xB28524EC, N4[9] = 0x18EC1701, N4[10]=0x5F1443AB, N4[11]=0x06B8E918;

    N6[0] = 0x00000006, N6[1] = 0x1E348000, N6[2]=0x20000003, N6[3] =0x8A442F99;
    N6[4] = 0x5C37B000, N6[5] = 0xB9B44D1E, N6[6]=0x05BE755A, N6[7] =0x9CD11BB2;
    N6[8] = 0x8BC7B762, N6[9] = 0xA5622282, N6[10]=0x8E9E6580, N6[11]=0x0A155DA4;

    if (projective384xyzz_Iszero(self)) {
       for (int i=0; i<LEN; i++) {
         self->x[i] = other->x[i];
         self->y[i] = other->y[i];
         self->zz[i] = other->zz[i];
         self->zzz[i] = other->zzz[i];
       }
       return;
    }
    if (projective384xyzz_Iszero(other)) {
      return;    
    }

    mul_big_384(self->zz, other->x, T0, N);
    mul_big_384(self->zzz, other->y, T1, N);
    mul_big_384(self->x, other->zz, self->x, N);
    mul_big_384(self->y, other->zzz, self->y, N);
    sub_big_384(T0, self->x, T0);
    add_big_384(T0, N6, T0);

    sub_big_384(T1, self->y, T1);
    add_big_384(T1, N4, T1);

    if ((is_zero_big_384_xyzz_(T0, temp_,sharedTemp)) && (is_zero_big_384_xyzz_(T1, temp_,sharedTemp))) {
        dbl = 1;
    }

    sqr_big_384(temp_, T0, temp, N);
    //case 5
	mul_big_384(self->zz, temp, self->zz, N);
    //case 6															//temp=T0∈ (4N,8N)
	mul_big_384(self->x, temp, temp_t, N);                              //Q = T0 = U1*PP			//T0=A*B∈ [0,2N)
    //case 7
	mul_big_384(T0, temp, B, N);                                        //PPP = B = P*PP			//B=A*B∈ [0,2N)
	add_big_384(B, temp_t, self->x);                                    //x1 = B + T0 = PPP + Q		//self.x=B+T0∈ [0,4N)
	add_big_384(self->x, temp_t, self->x);                              //x1 = PPP + 2Q				//self.x=self.x+T0∈ [0,6N)

    //case 8
	mul_big_384(self->zzz, B, self->zzz, N);                            //zzz1 = zzz1*PPP			//self.zzz=A*B∈ [0,2N)
    //case 9
	mul_big_384(self->y, B, self->y, N);                                //y1 = S1*PPP			    //self.y=A*B∈ [0,2N)
    //case 10
	mul_big_384(self->zz, other->zz, self->zz, N);                      //zz1 = zz1*PP*zz2			//self.zz=A*B∈ [0,2N)
    //case 11
	mul_big_384(self->zzz,other->zzz, self->zzz, N);                     //zzz1 = zzz1*zzz2			//self.zzz=A*B∈ [0,2N)
    //case 12
   // temp = A;   														 //temp=A∈ (2N,6N)
	sqr_big_384(temp_, T1, A, N);									     //temp=temp*temp∈ [0,2N)

	sub_big_384(A, self->x, self->x);     								 //self.x=T1-self.x∈ (-6N,2N)      
	add_big_384(self->x, N4, self->x);								     //self.x=self.x+4N∈ (-2N,6N)  

	sub_big_384(temp_t, self->x, T0);                                    //T0 = T0 - x1 = Q - X3	//T0=T0-self.x∈ (-6N,4N)  
	add_big_384(T0, N6, T0);											 //T0=T0+6N∈ (0,10N) 

    //case 13
	mul_big_384(T1, T0, T0, N);                                          //T0 = A * B = R*(Q - X3)			//T0∈ [0,2N)
    sub_big_384(T0, self->y, self->y);                                   //Y3 = y1 = R*(Q - X3) - S1*PPP  	//self.y=T0-self.y∈ (-2N,2N)
	add_big_384(self->y, N2, self->y);								     //self.y=self.y+4N∈ (0,4N)

	if(dbl != 1) {
       return;
	} else {
	   double_big_384(other->y, temp);								 //temp=2*temp∈ (0,8N)
	}

	//https://www.hyperelliptic.org/EFD/g1p/auto-shortw-xyzz.html#doubling-dbl-2008-s-1
	//U = 2*Y1
	//V = U2
	//W = U*V
	//S = X1*V
	//M = 3*X12+a*ZZ12
	//X3 = M2-2*S
	//Y3 = M*(S-X3)-W*Y1
	//ZZ3 = V*ZZ1
	//ZZZ3 = W*ZZZ1


	//case 14
	sqr_big_384(T1, temp, T0, N);              //T0 = U^2							//temp=temp^2∈ (0,2N)

	//case 15
	mul_big_384(temp, T0, T1, N);              //W = T1 = B*A=U^3					//T1=A*B∈ (0,2N)
	//case 16	
	mul_big_384(other->zz, T0, self->zz, N);   //ZZ3 = zz1 = zz2*V			        //self.zz=A*B∈ (0,2N)
	//case 17	
	mul_big_384(T1, other->zzz, self->zzz, N);  //ZZZ3 = zzz2 = W*zzz2		        //self.zzz∈ (0,2N)
	//case 18
	mul_big_384(T1, other->y, self->y, N);      //y1 = y2*W				            //self.y∈ (0,2N)
	//case 19
	mul_big_384(other->x, T0, T0, N);           //S = T0 = x2*V				        //T0∈ (0,2N)
	//case 20																
	sqr_big_384(temp_, other->x, T1, N);        //T1 = x2^2					//temp=temp*temp∈ (0,2N)														
	double_big_384(T1, temp);                   //A = 2T1 = 2*x2^2			//temp=2*temp∈ (0,4N)

	add_big_384(T1, temp, A);	  				//A=T1+A ∈ (0,6N)     

	//case 21
	sqr_big_384(temp_, A, self->x, N);          //x1 = M^2					//temp=temp^2∈ (0,2N)

	double_big_384(T0, T1);                     //T1 = 2*S					//temp=2*temp∈ (0,4N)
	sub_big_384(self->x, T0, self->x);          //X3 = x1 = M^2 - 2*S		//self.x=self.x-T0∈ (-2N,2N)
	add_big_384(self->x, N3, self->x);			//self.x=self.x+3N∈ (-N,5N)
	sub_big_384(T0, self->x, B);                //B = S - X3				//B=T0-self.x∈ (-5N,3N)
	add_big_384(B, N3, B);						//B=B+3N∈ (-2N,6N)
	//case 22
	mul_big_384(A, B, T0, N);                  //T0 = A*B = M*(S - X3)			//T0∈ (0,2N)

	sub_big_384(T0, self->y, self->y); 							    //self.y=T0-self.y∈ (-2N,2N)
	add_big_384(self->y, N2, self->y);								//self.y=self.y+2N∈ (0,4N)

}

 void  swj_mul_projective_xyzz(projective384xyzz *self, 
                                                         ulong *other,
                                                         int Ismont,
                                                         projective384xyzz *r,__local uint *sharedTemp)
{
   uint mont_one_384[12];
   mont_one_384[0] = 0xffffff68, mont_one_384[1] = 0x02cdffff, mont_one_384[2] = 0x7fffffb1;
   mont_one_384[3] = 0x51409f83, mont_one_384[4] = 0x8a7d3ff2, mont_one_384[5] = 0x9f7db3a9;
   mont_one_384[6] = 0x6e7c6305, mont_one_384[7] = 0x7b4e97b7, mont_one_384[8] = 0x803c84e8;
   mont_one_384[9] = 0x4cf495bf, mont_one_384[10] = 0xe2fdf49a, mont_one_384[11] = 0x008d6661;
   int found_one = 0;
   if (Ismont)
      big_to_repr_256(other);

   for (int i=0; i<12; i++) {
     r->x[i] = 0;
     r->y[i] = mont_one_384[i];
     r->zz[i] = 0;
     r->zzz[i] = 0;
   }
    uint tid = get_local_id(0);
//   if(tid == 0){
//     printf("temp: %u %u",sharedTemp[100],sharedTemp[1000]);
//   }
  
   for (int i = 255; i >= 0; i--) {     
      if (found_one == 0) {
         if (get_bit_256(other, i)) {
            found_one = 1;
         } else {
            continue;
         }
      }
      swj_double_in_place_xyzz(r);
      // swj_double_in_place_xyzz(self); 
      if (get_bit_256(other, i)) {
            swj_add_assign_xyzz(r, self,sharedTemp);
      }
    }
}

 void affine384_to_projective384_xyzz(affine384 *a, projective384xyzz *b)
{
    uint one[12];
    one[0] = 0xffffff68, one[1] = 0x02cdffff, one[2] = 0x7fffffb1;
    one[3] = 0x51409f83, one[4] = 0x8a7d3ff2, one[5] = 0x9f7db3a9;
    one[6] = 0x6e7c6305, one[7] = 0x7b4e97b7, one[8] = 0x803c84e8;
    one[9] = 0x4cf495bf, one[10] = 0xe2fdf49a, one[11] = 0x008d6661;
    uint mont_zero_384[12];
    mont_zero_384[0]= 0x00000000, mont_zero_384[1]= 0x00000000, mont_zero_384[2]= 0x00000000;
    mont_zero_384[3]= 0x00000000, mont_zero_384[4]= 0x00000000, mont_zero_384[5]= 0x00000000;
    mont_zero_384[6]= 0x00000000, mont_zero_384[7]= 0x00000000, mont_zero_384[8]= 0x00000000;
    mont_zero_384[9]= 0x00000000, mont_zero_384[10]= 0x00000000, mont_zero_384[11]= 0x00000000;
    if (affine384_Iszero(a)) {
      for (int i=0; i<12; i++) {
        b->x[i]= mont_zero_384[i];
        b->y[i]= one[i];
        b->zz[i]= mont_zero_384[i];
        b->zzz[i]= mont_zero_384[i];
      }
    } else {  
      for (int i=0; i<12; i++) {
        b->x[i]= a->x[i];
        b->y[i]= a->y[i];
        b->zz[i]= one[i];
        b->zzz[i]= one[i];
      }
    }
  }

 void projectivexyzzReduce(projective384xyzz* self,__local uint *sharedTemp) {
     uint temp[12];
     xyzzReduce_(self->x, self->x, temp,sharedTemp);
     xyzzReduce_(self->y, self->y, temp,sharedTemp);
     xyzzReduce_(self->zz, self->zz, temp,sharedTemp);
     xyzzReduce_(self->zzz, self->zzz, temp,sharedTemp);
  }

  /** swj_compute.cu end*/


/** blake2s start*/
static inline uint ROTR32( const uint w, const unsigned c )
{
  return ( w >> c ) | ( w << ( 32 - c ) );
}

// state context
typedef struct Blake2sCTX{
    char b[64];                      // input buffer
    uint h[8];                      // chained state
    uint t[2];                      // total number of bytes
    int c;                           // pointer for b[]
    int outlen;                      // digest size
} blake2s_ctx;


// Little-endian byte access.
static inline uint B2S_GET32( const void *src )
{
#if defined(__ENDIAN_LITTLE__)
  return *( uint * )( src );
#else
  const uchar *p = ( uchar * )src;
  uint w = *p++;
  w |= ( uint )( *p++ ) <<  8;
  w |= ( uint )( *p++ ) << 16;
  w |= ( uint )( *p++ ) << 24;
  return w;
#endif
}

__constant static const uchar blakes_sigma[12][16] =
{
{ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 },
        { 14, 10, 4, 8, 9, 15, 13, 6, 1, 12, 0, 2, 11, 7, 5, 3 },
        { 11, 8, 12, 0, 5, 2, 15, 13, 10, 14, 3, 6, 7, 1, 9, 4 },
        { 7, 9, 3, 1, 13, 12, 11, 14, 2, 6, 5, 10, 4, 0, 15, 8 },
        { 9, 0, 5, 7, 2, 4, 10, 15, 14, 1, 11, 12, 6, 8, 3, 13 },
        { 2, 12, 6, 10, 0, 11, 8, 3, 4, 13, 7, 5, 15, 14, 1, 9 },
        { 12, 5, 1, 15, 14, 13, 4, 10, 0, 7, 6, 3, 9, 2, 8, 11 },
        { 13, 11, 7, 14, 12, 1, 3, 9, 5, 0, 15, 4, 8, 6, 2, 10 },
        { 6, 15, 14, 9, 11, 3, 0, 8, 12, 2, 13, 7, 1, 4, 10, 5 },
        { 10, 2, 8, 4, 7, 6, 1, 5, 15, 11, 9, 14, 3, 12, 13, 0 }
};

// Initialization Vector.

__constant static const uint blake2s_iv[8] =
{
    0x6A09E667, 0xBB67AE85, 0x3C6EF372, 0xA54FF53A,
    0x510E527F, 0x9B05688C, 0x1F83D9AB, 0x5BE0CD19
};

// Compression function. "last" flag indicates last block.

static void blake2s_compress(blake2s_ctx *ctx, int last)
{
    int i;
    uint v[16], m[16];

    for (i = 0; i < 8; i++) {           // init work variables
        v[i] = ctx->h[i];
        v[i + 8] = blake2s_iv[i];
    }

    v[12] ^= ctx->t[0];                 // low 32 bits of offset
    v[13] ^= ctx->t[1];                 // high 32 bits
    if (last)                           // last block flag set ?
        v[14] = ~v[14];

    for (i = 0; i < 16; i++)            // get little-endian words
        m[i] = B2S_GET32(&ctx->b[4 * i]);
// Mixing function G.

    #define B2S_G(a, b, c, d, x, y) {   \
        v[a] = v[a] + v[b] + x;         \
        v[d] = ROTR32(v[d] ^ v[a], 16); \
        v[c] = v[c] + v[d];             \
        v[b] = ROTR32(v[b] ^ v[c], 12); \
        v[a] = v[a] + v[b] + y;         \
        v[d] = ROTR32(v[d] ^ v[a], 8);  \
        v[c] = v[c] + v[d];             \
        v[b] = ROTR32(v[b] ^ v[c], 7); }
    for (i = 0; i < 10; i++) {          // ten rounds
        B2S_G( 0, 4,  8, 12, m[blakes_sigma[i][ 0]], m[blakes_sigma[i][ 1]]);
        B2S_G( 1, 5,  9, 13, m[blakes_sigma[i][ 2]], m[blakes_sigma[i][ 3]]);
        B2S_G( 2, 6, 10, 14, m[blakes_sigma[i][ 4]], m[blakes_sigma[i][ 5]]);
        B2S_G( 3, 7, 11, 15, m[blakes_sigma[i][ 6]], m[blakes_sigma[i][ 7]]);
        B2S_G( 0, 5, 10, 15, m[blakes_sigma[i][ 8]], m[blakes_sigma[i][ 9]]);
        B2S_G( 1, 6, 11, 12, m[blakes_sigma[i][10]], m[blakes_sigma[i][11]]);
        B2S_G( 2, 7,  8, 13, m[blakes_sigma[i][12]], m[blakes_sigma[i][13]]);
        B2S_G( 3, 4,  9, 14, m[blakes_sigma[i][14]], m[blakes_sigma[i][15]]);
    }

    for( i = 0; i < 8; ++i )
        ctx->h[i] ^= v[i] ^ v[i + 8];
}

// Add "inlen" bytes from "in" into the hash.

void blake2s_update(blake2s_ctx *ctx,
    const void *in, int inlen)       // data bytes
{
    int i;

    for (i = 0; i < inlen; i++) {
        if (ctx->c == 64) {             // buffer full ?
            ctx->t[0] += ctx->c;        // add counters
            if (ctx->t[0] < ctx->c)     // carry overflow ?
                ctx->t[1]++;            // high word
            blake2s_compress(ctx, 0);   // compress (not last)
            ctx->c = 0;                 // counter to zero
        }
        ctx->b[ctx->c++] = ((const uchar *) in)[i];
    }
}

// Initialize the hashing context "ctx" with optional key "key".
//      1 <= outlen <= 32 gives the digest size in bytes.
//      Secret key (also <= 32 bytes) is optional (keylen = 0).

int blake2s_init(blake2s_ctx *ctx, int outlen,
    const void *key, int keylen)     // (keylen=0: no key)
{
    int i;

    if (outlen == 0 || outlen > 32 || keylen > 32)
        return -1;                      // illegal parameters

    for (i = 0; i < 8; i++)             // state, "param block"
        ctx->h[i] = blake2s_iv[i];
    ctx->h[0] ^= 0x01010000 ^ (keylen << 8) ^ outlen;

    ctx->t[0] = 0;                      // input count low word
    ctx->t[1] = 0;                      // input count high word
    ctx->c = 0;                         // pointer within buffer
    ctx->outlen = outlen;

    for (i = keylen; i < 64; i++)       // zero input block
        ctx->b[i] = 0;
    if (keylen > 0) {
        blake2s_update(ctx, key, keylen);
        ctx->c = 64;                    // at the end
    }

    return 0;
}

static inline void swap(uchar* arr, int len)
{
  uchar t;
  for (int i = 0;i < len / 2;i++) {
    t = arr[i];
    arr[i] = arr[len-1-i];
    arr[len-1-i] = t;   
  }
}
uint byte_reverse_32(uint num) {
        union bytes {
        uchar b[4];
        uint n;
    } bytes;
    bytes.n = num;

    uint ret = 0;
    ret |= bytes.b[0] << 24;
    ret |= bytes.b[1] << 16;
    ret |= bytes.b[2] << 8;
    ret |= bytes.b[3];

    return ret;
}
// Generate the message digest (size given in init).
//      Result placed in "out". void *out

void blake2s_final(blake2s_ctx *ctx)
{
    // int i;

    ctx->t[0] += ctx->c;                // mark last block offset
    if (ctx->t[0] < ctx->c)             // carry overflow
        ctx->t[1]++;                    // high word

    while (ctx->c < 64)                 // fill up with zeros
        ctx->b[ctx->c++] = 0;
    blake2s_compress(ctx, 1);           // final block flag = 1
    // for(int i=0;i<8;i++){
    //     // printf("xxxxx0 %x",ctx->h[i]);
    //     ctx->h[i] = byte_reverse_32(ctx->h[i]);
    //     // printf("xxxxx1 %x",ctx->h[i]);
    // }
    // little endian convert and store
    // for (i = 0; i < ctx->outlen; i++) {
    //     ((uchar *) out)[i] =
    //         (ctx->h[i >> 2] >> (8 * (i & 3))) & 0xFF;
    // }
}
/** blake2s end*/

/** blake2b start*/

static inline ulong ROTR64( const ulong w, const unsigned c )
{
  return ( w >> c ) | ( w << ( 64 - c ) );
}

// state context
typedef struct Blake2bCTX{
    char b[128];                     // input buffer
    ulong h[8];                      // chained state
    ulong t[2];                      // total number of bytes
    int c;                           // pointer for b[]
    int outlen;                      // digest size
} blake2b_ctx;

// Little-endian byte access.
static inline ulong B2B_GET64( const void *src )
{
#if defined(__ENDIAN_LITTLE__)
  return *( ulong * )( src );
#else
  const uchar *p = ( uchar * )src;
  ulong w = *p++;
  w |= ( ulong )( *p++ ) <<  8;
  w |= ( ulong )( *p++ ) << 16;
  w |= ( ulong )( *p++ ) << 24;
  w |= ( ulong )( *p++ ) << 32;
  w |= ( ulong )( *p++ ) << 40;
  w |= ( ulong )( *p++ ) << 48;
  w |= ( ulong )( *p++ ) << 56;
  return w;
#endif
}



// Initialization Vector.

__constant static const ulong blake2b_iv[8] = {
    0x6A09E667F3BCC908, 0xBB67AE8584CAA73B,
    0x3C6EF372FE94F82B, 0xA54FF53A5F1D36F1,
    0x510E527FADE682D1, 0x9B05688C2B3E6C1F,
    0x1F83D9ABFB41BD6B, 0x5BE0CD19137E2179
};

__constant static const uchar sigma[12][16] =
{
  {  0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15 } ,
  { 14, 10,  4,  8,  9, 15, 13,  6,  1, 12,  0,  2, 11,  7,  5,  3 } ,
  { 11,  8, 12,  0,  5,  2, 15, 13, 10, 14,  3,  6,  7,  1,  9,  4 } ,
  {  7,  9,  3,  1, 13, 12, 11, 14,  2,  6,  5, 10,  4,  0, 15,  8 } ,
  {  9,  0,  5,  7,  2,  4, 10, 15, 14,  1, 11, 12,  6,  8,  3, 13 } ,
  {  2, 12,  6, 10,  0, 11,  8,  3,  4, 13,  7,  5, 15, 14,  1,  9 } ,
  { 12,  5,  1, 15, 14, 13,  4, 10,  0,  7,  6,  3,  9,  2,  8, 11 } ,
  { 13, 11,  7, 14, 12,  1,  3,  9,  5,  0, 15,  4,  8,  6,  2, 10 } ,
  {  6, 15, 14,  9, 11,  3,  0,  8, 12,  2, 13,  7,  1,  4, 10,  5 } ,
  { 10,  2,  8,  4,  7,  6,  1,  5, 15, 11,  9, 14,  3, 12, 13 , 0 } ,
  {  0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15 } ,
  { 14, 10,  4,  8,  9, 15, 13,  6,  1, 12,  0,  2, 11,  7,  5,  3 }
};

// Compression function. "last" flag indicates last block.

static void blake2b_compress(blake2b_ctx *ctx, int last)
{
    int i;
    ulong v[16], m[16];

    for (i = 0; i < 8; i++) {           // init work variables
        v[i] = ctx->h[i];
        v[i + 8] = blake2b_iv[i];
    }

    v[12] ^= ctx->t[0];                 // low 64 bits of offset
    v[13] ^= ctx->t[1];                 // high 64 bits
    if (last)                           // last block flag set ?
        v[14] = ~v[14];

    // G Mixing function.
    #define B2B_G(a, b, c, d, x, y) {   \
        v[a] = v[a] + v[b] + x;         \
        v[d] = ROTR64(v[d] ^ v[a], 32); \
        v[c] = v[c] + v[d];             \
        v[b] = ROTR64(v[b] ^ v[c], 24); \
        v[a] = v[a] + v[b] + y;         \
        v[d] = ROTR64(v[d] ^ v[a], 16); \
        v[c] = v[c] + v[d];             \
        v[b] = ROTR64(v[b] ^ v[c], 63); }
    for (i = 0; i < 16; i++)            // get little-endian words
        m[i] = B2B_GET64(&ctx->b[8 * i]);

    for (i = 0; i < 12; i++) {          // twelve rounds
        B2B_G( 0, 4,  8, 12, m[sigma[i][ 0]], m[sigma[i][ 1]]);
        B2B_G( 1, 5,  9, 13, m[sigma[i][ 2]], m[sigma[i][ 3]]);
        B2B_G( 2, 6, 10, 14, m[sigma[i][ 4]], m[sigma[i][ 5]]);
        B2B_G( 3, 7, 11, 15, m[sigma[i][ 6]], m[sigma[i][ 7]]);
        B2B_G( 0, 5, 10, 15, m[sigma[i][ 8]], m[sigma[i][ 9]]);
        B2B_G( 1, 6, 11, 12, m[sigma[i][10]], m[sigma[i][11]]);
        B2B_G( 2, 7,  8, 13, m[sigma[i][12]], m[sigma[i][13]]);
        B2B_G( 3, 4,  9, 14, m[sigma[i][14]], m[sigma[i][15]]);
    }

    for( i = 0; i < 8; ++i )
        ctx->h[i] ^= v[i] ^ v[i + 8];
}


// Add "inlen" bytes from "in" into the hash.

void blake2b_update(blake2b_ctx *ctx,
    const void *in, int inlen)       // data bytes
{
    int i;

    for (i = 0; i < inlen; i++) {
        if (ctx->c == 128) {            // buffer full ?
            ctx->t[0] += ctx->c;        // add counters
            if (ctx->t[0] < ctx->c)     // carry overflow ?
                ctx->t[1]++;            // high word
            blake2b_compress(ctx, 0);   // compress (not last)
            ctx->c = 0;                 // counter to zero
        }
        ctx->b[ctx->c++] = ((const char *) in)[i];
    }
}

// Initialize the hashing context "ctx" with optional key "key".
//      1 <= outlen <= 64 gives the digest size in bytes.
//      Secret key (also <= 64 bytes) is optional (keylen = 0).

int blake2b_init(blake2b_ctx *ctx, int outlen,
    const void *key, int keylen)        // (keylen=0: no key)
{
    int i;

    if (outlen == 0 || outlen > 64 || keylen > 64)
        return -1;                      // illegal parameters

    for (i = 0; i < 8; i++)             // state, "param block"
        ctx->h[i] = blake2b_iv[i];
    ctx->h[0] ^= 0x01010000 ^ (keylen << 8) ^ outlen;

    ctx->t[0] = 0;                      // input count low word
    ctx->t[1] = 0;                      // input count high word
    ctx->c = 0;                         // pointer within buffer
    ctx->outlen = outlen;

    for (i = keylen; i < 128; i++)      // zero input block
        ctx->b[i] = 0;
    if (keylen > 0) {
        blake2b_update(ctx, key, keylen);
        ctx->c = 128;                   // at the end
    }

    return 0;
}



// Generate the message digest (size given in init).
//      Result placed in "out". void *out

void blake2b_final(blake2b_ctx *ctx)
{
    // int i;

    ctx->t[0] += ctx->c;                // mark last block offset
    if (ctx->t[0] < ctx->c)             // carry overflow
        ctx->t[1]++;                    // high word

    while (ctx->c < 128)                // fill up with zeros
        ctx->b[ctx->c++] = 0;
    blake2b_compress(ctx, 1);           // final block flag = 1
    // for(int i=0;i<8;i++){
    //     ctx->h[i] = byte_reverse_32(ctx->h[i]);
    // }
    // little endian convert and store
    // for (i = 0; i < ctx->outlen; i++) {
    //     ((char *) out)[i] =
    //         (ctx->h[i >> 3] >> (8 * (i & 7))) & 0xFF;
    // }
}
/** blake2b end*/



void memcpy(uchar * dest,uchar *src ,int l){
    for (int i=0;i<l;i++){
        dest[i] = src[i];
    }
}

static void from_bytes_be_mod_order(ulong* data_,
                                                                 int len,
                                                                 uint *r)
{
    // uchar *hash = (uchar *)data_;
    //     for(int i=0;i<64;i++){
    //         printf("==%02x",hash[i]);
    //     }
    //    return;
  uchar new_data1[64];
  uchar new_data2[64];
  uint res[8], res2[8];
  uint yc1[8], yc0[8];
  uint COEFF_2POW256[8];
  uint N[8];
  uchar data[64];
  COEFF_2POW256[0] = 0xb861857b, COEFF_2POW256[1] = 0x25d577ba;
  COEFF_2POW256[2] = 0x8860591f, COEFF_2POW256[3] = 0xcc2c27b5;
  COEFF_2POW256[4] = 0xe5dc8593, COEFF_2POW256[5] = 0xa7cc008f;
  COEFF_2POW256[6] = 0xeff1c939, COEFF_2POW256[7] = 0x011fdae7;
  N[0] = 0x00000001, N[1] = 0xa118000;
  N[2] = 0xd0000001, N[3] = 0x59aa76fe;
  N[4] = 0x5c37b001, N[5] = 0x60b44d1e;
  N[6] = 0x9a2ca556, N[7] = 0x12ab655e;

  for (int i=0; i<8; i++) {
    data[i*8+0] = (uchar)(data_[i] >> 0);
    data[i*8+1] = (uchar)(data_[i] >> 8);
    data[i*8+2] = (uchar)(data_[i] >> 16);
    data[i*8+3] = (uchar)(data_[i] >> 24);
    data[i*8+4] = (uchar)(data_[i] >> 32);
    data[i*8+5] = (uchar)(data_[i] >> 40);
    data[i*8+6] = (uchar)(data_[i] >> 48);
    data[i*8+7] = (uchar)(data_[i] >> 56);
  }

  swap(data, len);

  //num_modulus_bytes = 32
  int num_modulus_bytes = (MODULUS_BITS_G + 7) / 8;
  
  int num_bytes_to_directly_convert = num_modulus_bytes - 1 > len ? len : num_modulus_bytes - 1;
  memcpy(new_data1, data, num_bytes_to_directly_convert);
  //31byte整体翻转
  swap(new_data1, num_bytes_to_directly_convert);

  memcpy(new_data2, &data[num_bytes_to_directly_convert], 64 - num_bytes_to_directly_convert);
  //33byte整体翻转
  swap(new_data2, 64 - num_bytes_to_directly_convert);

  memcpy(data,new_data2,64 - num_bytes_to_directly_convert);
  memcpy(&data[64 - num_bytes_to_directly_convert],new_data1,num_bytes_to_directly_convert);
  //用31个byte头一次，后面用33次把每个byte消化掉,变成31byte 处理 31byte 处理2byte
  // memcpy(&yc0[0], &data[0], 32);
  for(int i=0; i<8; i++) {
     yc0[i] = B2S_GET32(&data[i*4]);
     yc1[i] = B2S_GET32(&data[32+i*4]);
  }

  big_from_repr_256(&yc1[0], res); 
  big_from_repr_256(&yc0[0], res2); 
  mul_assign_big_256(res, COEFF_2POW256, res, N);
  add_assign_big_256(res, res2, r, N);

}

 /** 外部调用kernel hash_to_polynomial 结果转小整型 uint*/
__kernel void hash_to_polynomial(__global uint *data, __global uint *var, __global uint* coef) {
    blake2s_ctx S_s;
    blake2b_ctx S_b;
    uint tid = get_local_id(0);
    uint bid = get_group_id(0);
    uint tid_ = get_global_id(0);
    uchar input_with_counter[128];
    uint coef_[8];
    uint len = 0;
    uint len_ = 0;
    uint sm = get_num_groups(0);
    uint threads = core_per_sm;
#if USE_VAR == 1    
    uint evaluation_size = device_evaluation_size;
#endif    
    uint msg[data_alloc_len_/4];
    // blake2s_state S_s;
    // blake2b_state S_b;
    len = var[0];
    int size = 0;
    len_ = len >> 2;
    for (int i=0; i<data_alloc_len_/4; i++) {
        msg[i] = data[bid*threads*len_+tid*len_+i];
    }
    uchar * key;
    blake2s_init(&S_s, 32, key, 0);
    blake2s_update(&S_s, msg, len);
    blake2s_final(&S_s);
    // blake2s_init(&S_s, BLAKE2S_OUTBYTES, 0, blake2s_IV);
    // blake_2s(&S_s, msg, len, 0);
    memcpy(input_with_counter, S_s.h, 32);

    for (int i=0; i<degree_;  i++) {
       *(int*)&input_with_counter[32] = i;
    //    blake2b_init(&S_b, BLAKE2B_OUTBYTES, 0, blake2b_IV); 
    //    blake_2b(&S_b, input_with_counter, 36, 0);
        blake2b_init(&S_b, 64, key, 0);
       blake2b_update(&S_b, input_with_counter, 36);
       blake2b_final(&S_b);

        // uchar *hash = (uchar *)S_b.h;
        // printf("%02x%02x%02x%02x",hash[0],hash[1],hash[2],hash[3]);
        // return;
       from_bytes_be_mod_order(&S_b.h[0], 64, coef_);
       for (int j=0; j<8; j++) {
#if 1       
#if USE_VAR == 1 
          coef[bid*threads*evaluation_size*8+ 
                             tid*evaluation_size*8 +
                             i*8+j] = coef_[j];
#else
          coef[bid*threads*Evaluations_size_*8+ 
                             tid*Evaluations_size_*8 +
                             i*8+j] = coef_[j];
#endif
#else
#if USE_VAR == 1 
        coef[j*sm*threads*evaluation_size+
                           i*sm*threads+tid_] = coef_[j];
#else 
        coef[j*sm*threads*Evaluations_size_+
                           i*sm*threads+tid_] = coef_[j];
#endif
#endif
       }
       //(TODO): add write size code
       //if (!is_zero_big_256(coef_))
       //    size++;
    }
#if USE_VAR == 1
    for (int i=0; i<evaluation_size - degree_;  i++) {
#else    
    for (int i=0; i<Evaluations_size_ - degree_;  i++) {
#endif      
       for (int j=0; j<8; j++) {
#if 1        
#if USE_VAR == 1
          coef[bid*threads*evaluation_size*8 + 
                             tid*evaluation_size*8 +
                             degree_*8+i*8+j] = 0;
#else
          coef[bid*threads*Evaluations_size_*8 + 
                             tid*Evaluations_size_*8 +
                             degree_*8+i*8+j] = 0;
#endif                             
#else 
#if USE_VAR == 1
         coef[j*sm*threads*evaluation_size+
                           (degree_+i)*sm*threads+tid_] = coef_[j];
#else
         coef[j*sm*threads*Evaluations_size_+
                           (degree_+i)*sm*threads+tid_] = coef_[j];
#endif                           
#endif
       }
    }
    //(TODO): add write size code
    /* for(int i=0; i<16*1024*8; i++){
      printf("coef[%d] is %x\n",i, ((uint *)coef)[i]);
    }*/
}


// 蒙哥马利 hash
 /** 外部调用kernel hash_to_polynomial_m 结果转大整型 uint64*/
__kernel void hash_to_polynomial_m(global uint *data,global uint *var, global ulong* coef) {

    blake2s_ctx S_s;
    blake2b_ctx S_b;
    uint msg[data_alloc_len_/4];
    
    uint tid = get_local_id(0); // x y z => 0 1 2
    uint bid = get_group_id(0);
    uint tid_ = get_global_id(0);
    uchar input_with_counter[128];
    uint threads = core_per_sm;
    uint coef_[8];
    uint len = 0;
    uint len_ = 0;
#if USE_VAR == 1
    uint evaluation_size = device_evaluation_size;
#endif
    uint sm = get_num_groups(0);

    ulong coef_t;
    len = var[0];
    int size = 0;
    len_ = len >> 2;
    for (int i=0; i<data_alloc_len_/4; i++) {
        msg[i] = data[bid*threads*len_+tid*len_+i];
        /*if (get_local_id(0) == 0 && get_group_id(0) == 0) {
          printf("msg[%d] is %x\n",i, msg[i]);
        }*/
    }
    /* if (get_local_id(0) == 0 && get_group_id(0) == 0) {
       for (int i=0; i<8*32; i++) {
          printf("data[%d] is %x\n",i, ((uint *)data)[i]);
       }
       printf("len is %d\n", ((uint *)var)[0]);
    }*/
    uchar * key;
    blake2s_init(&S_s, 32, key, 0);
    blake2s_update(&S_s, msg, len);
    blake2s_final(&S_s);
    // blake2s_init(&S_s, BLAKE2S_OUTBYTES, 0, blake2s_IV);
    // blake_2s(&S_s, msg, len, 0);
    // uchar *hash ;
    // for(int i=0;i<32;i++){
    //     printf("%02x",hash[i]);
    // }
    // hash = (uchar *)S_s.h;
    // printf("%x%x%x%x%x%x%x%x",hash[0],hash[1],hash[2],hash[3],hash[4],hash[5],hash[6],hash[7]);
    // return;
    
    memcpy((uchar *)input_with_counter, (uchar *)S_s.h, 32);

    for (int i=0; i<degree_;  i++) {
       *(int*)&input_with_counter[32] = i;
       blake2b_init(&S_b, 64, key, 0);
       blake2b_update(&S_b, input_with_counter, 36);
       blake2b_final(&S_b);
    //    hash = (uchar *)S_b.h;
    //    if( i==0 ){
    //         printf("blake2b: %02x%02x%02x%02x%02x%02x%02x%02x",hash[0],hash[1],hash[2],hash[3],hash[4],hash[5],hash[6],hash[7]);
    //    }
        
        // for(int j=0;j<64;j++){
            // printf("=%02x",hash[j]);
            // hash1[i*64+j] = hash[j];
        // }

    //    blake2b_init(&S_b, BLAKE2B_OUTBYTES, 0, blake2b_IV); 
    //    blake_2b(&S_b, input_with_counter, 36, 0);
       from_bytes_be_mod_order(&S_b.h[0], 64, coef_);
        // if( i== 800){
            // hash = (uchar *)coef_;
            // for(int k=0;k<32;k++){
                 
            //     resF[i*32+k] = hash[k];

            //     // if( i== 0){
            //     //     printf("%02x",resF[k]);
            //     //  }
            // }
            // if( i==0 ){
            //     printf("from_bytes_be_mod_order: %02x%02x%02x%02x%02x%02x%02x%02x",hash[0],hash[1],hash[2],hash[3],hash[4],hash[5],hash[6],hash[7]);
            // }
        // }
        
       
       for (int j=0; j<4; j++) {
#if 0        
          coef[bid*threads*Evaluations_size_*8+ 
                             tid*Evaluations_size_*8 +
                             i*8+j] = coef_[j];
#else  
         coef_t = big_make_wide(coef_[2*j], coef_[2*j+1]);
         coef[i*4*sm*threads+j*sm*threads+tid_] = coef_t;
         /*if (get_local_id(0) < 2 && get_group_id(0) == 0) {
            printf("threadIdx is %d\n",threadIdx); 
            printf("coef_t[%d] is %lx\n", i*4+j, coef_t);
         }*/
#endif
       }
       //(TODO): add write size code
       //if (!is_zero_big_256(coef_))
       //    size++;
    }
#if USE_VAR==1
    for (int i=0; i<evaluation_size - degree_;  i++) {
#else    
    for (int i=0; i<Evaluations_size_ - degree_;  i++) {
#endif
       for (int j=0; j<4; j++) {
#if 0        
          coef[bid*threads*Evaluations_size_*8 + 
                             tid*Evaluations_size_*8 +
                             degree_*8+i*8+j] = 0;
#else    
         coef_t = big_make_wide(coef_[2*j], coef_[2*j+1]);
         coef[(degree_+i)*sm*threads*4 +
                             j*sm*threads+tid_] = 0;

#endif
       }
    }
    //(TODO): add write size code
    /* for(int i=0; i<16*1024*8; i++){
      printf("coef[%d] is %x\n",i, ((uint *)coef)[i]);
    }*/

}
/** libblake2b.cu end */


/** ftt.cu start */

__constant uint ModuleTable[192] = {
	(uint)0x00000000,
    (uint)0x00000000,
    (uint)0x00000000,
    (uint)0x00000000,
    (uint)0x00000000,
    (uint)0x00000000,
	(uint)0x00000000,
    (uint)0x00000000,
	(uint)0x00000000,
    (uint)0x00000000,
    (uint)0x00000000,
    (uint)0x00000000,
//N
    (uint)0x00000001,
    (uint)0x8508C000,
    (uint)0x30000000,
    (uint)0x170B5D44,
    (uint)0xBA094800,
    (uint)0x1EF3622F,
    (uint)0x00F5138F,
    (uint)0x1A22D9F3,
    (uint)0x6CA1493B,
    (uint)0xC63B05C0,
    (uint)0x17C510EA,
    (uint)0x01AE3A46,
//2N
    (uint)0x00000002,
    (uint)0x0A118000,
    (uint)0x60000001,
    (uint)0x2E16BA88,
    (uint)0x74129000,
    (uint)0x3DE6C45F,
    (uint)0x01EA271E,
    (uint)0x3445B3E6,
    (uint)0xD9429276,
    (uint)0x8C760B80,
    (uint)0x2F8A21D5,
    (uint)0x035C748C,
//3N
    (uint)0x00000003,
    (uint)0x8F1A4000,
    (uint)0x90000001,
    (uint)0x452217CC,
    (uint)0x2E1BD800,
    (uint)0x5CDA268F,
    (uint)0x02DF3AAD,
    (uint)0x4E688DD9,
    (uint)0x45E3DBB1,
    (uint)0x52B11141,
    (uint)0x474F32C0,
    (uint)0x050AAED2,
//4N
    (uint)0x00000004,
    (uint)0x14230000,
    (uint)0xC0000002,
    (uint)0x5C2D7510,
    (uint)0xE8252000,
    (uint)0x7BCD88BE,
    (uint)0x03D44E3C,
    (uint)0x688B67CC,
    (uint)0xB28524EC,
    (uint)0x18EC1701,
    (uint)0x5F1443AB,
    (uint)0x06B8E918,
//5N
    (uint)0x00000005,
    (uint)0x992BC000,
    (uint)0xF0000002,
    (uint)0x7338D254,
    (uint)0xA22E6800,
    (uint)0x9AC0EAEE,
    (uint)0x04C961CB,
    (uint)0x82AE41BF,
    (uint)0x1F266E27,
    (uint)0xDF271CC2,
    (uint)0x76D95495,
    (uint)0x0867235E,
//6N
    (uint)0x00000006,
    (uint)0x1E348000,
    (uint)0x20000003,
    (uint)0x8A442F99,
    (uint)0x5C37B000,
    (uint)0xB9B44D1E,
    (uint)0x05BE755A,
    (uint)0x9CD11BB2,
    (uint)0x8BC7B762,
    (uint)0xA5622282,
    (uint)0x8E9E6580,
    (uint)0x0A155DA4,
//7N
    (uint)0x00000007,
    (uint)0xA33D4000,
    (uint)0x50000003,
    (uint)0xA14F8CDD,
    (uint)0x1640F800,
    (uint)0xD8A7AF4E,
    (uint)0x06B388E9,
    (uint)0xB6F3F5A5,
    (uint)0xF869009D,
    (uint)0x6B9D2842,
    (uint)0xA663766B,
    (uint)0x0BC397EA,
//8N
    (uint)0x00000008,
    (uint)0x28460000,
    (uint)0x80000004,
	(uint)0xB85AEA21,
	(uint)0xD04A4000,
	(uint)0xF79B117D,
	(uint)0x07A89C78,
	(uint)0xD116CF98,
	(uint)0x650A49D8,
	(uint)0x31D82E03,
	(uint)0xBE288756,
	(uint)0x0D71D230,
//9N    
    (uint)0x00000009,
    (uint)0xAD4EC000,
    (uint)0xB0000004,
    (uint)0xCF664765,
    (uint)0x8A538800,
	(uint)0x168E73AD,
    (uint)0x089DB008,
	(uint)0xEB39A98B,
    (uint)0xD1AB9313,
	(uint)0xF81333C3,
    (uint)0xD5ED9840,
	(uint)0x0F200C76,
//10N
    (uint)0x0000000A, 
    (uint)0x32578000,
    (uint)0xE0000005,
	(uint)0xE671A4A9,
    (uint)0x445CD000,
	(uint)0x3581D5DD,
    (uint)0x0992C397,
	(uint)0x055C837E,
    (uint)0x3E4CDC4F,
	(uint)0xBE4E3984,
    (uint)0xEDB2A92B,
	(uint)0x10CE46BC,
//11N
    (uint)0x0000000B,
    (uint)0xB7604000,
    (uint)0x10000005,
    (uint)0xFD7D01EE,
    (uint)0xFE661800,
    (uint)0x5475380C,
    (uint)0x0A87D726,
	(uint)0x1F7F5D71,
    (uint)0xAAEE258A,
	(uint)0x84893F44,
    (uint)0x0577BA16,
	(uint)0x127C8103,
//12N
    (uint)0x0000000C,
    (uint)0x3C690000,
    (uint)0x40000006,
	(uint)0x14885F32,
    (uint)0xB86F6001,
    (uint)0x73689A3C,
    (uint)0x0B7CEAB5,
	(uint)0x39A23764,
    (uint)0x178F6EC5,
	(uint)0x4AC44505,
    (uint)0x1D3CCB01,
	(uint)0x142ABB49,
 //13N
    (uint)0x0000000D,
    (uint)0xC171C000,
    (uint)0x70000006,
    (uint)0x2B93BC76,
    (uint)0x7278A801,
	(uint)0x925BFC6C,
    (uint)0x0C71FE44,
	(uint)0x53C51157,
    (uint)0x8430B800,
	(uint)0x10FF4AC5,
    (uint)0x3501DBEC,
	(uint)0x15D8F58F,
//14N
    (uint)0x0000000E,
    (uint)0x467A8000,
    (uint)0xA0000007,
	(uint)0x429F19BA,
    (uint)0x2C81F001,
	(uint)0xB14F5E9C,
    (uint)0x0D6711D3,
	(uint)0x6DE7EB4A,
    (uint)0xF0D2013B,
	(uint)0xD73A5085,
    (uint)0x4CC6ECD6,
	(uint)0x17872FD5,
//15N
    (uint)0x0000000F,
	(uint)0xCB834000,
    (uint)0xD0000007,
	(uint)0x59AA76FE,
    (uint)0xE68B3801,
	(uint)0xD042C0CB,
    (uint)0x0E5C2562,
	(uint)0x880AC53D,
    (uint)0x5D734A76,
	(uint)0x9D755646,
    (uint)0x648BFDC1,
	(uint)0x19356A1B
};

static uint reverseBits(uint n)
{
  n = ((n >> 1) & 0x55555555) | ((n & 0x55555555) << 1);
  n = ((n >> 2) & 0x33333333) | ((n & 0x33333333) << 2);
  n = ((n >> 4) & 0x0f0f0f0f) | ((n & 0x0f0f0f0f) << 4);
  n = ((n >> 8) & 0x00ff00ff) | ((n & 0x00ff00ff) << 8);
  return n >> 16 | n << 16;
}

kernel void NTT_IO(__global uint *var, __global uint *g, __global uint* var_)
{ 
  uint tid = get_global_id(0);
  uint low[8], high[8], neg[8], root[8], temp[8];
  uint N[8];
  uint stride = 8192;
  uint i;
  uint g_index;
  uint index;
  uint r, k;
  uint M, r_val, r_s_val, o_low, o_high;
  uint threads;
  uint outer_times;
  N[0]= 0x00000001, N[1]= 0x0a118000, N[2]= 0xd0000001, N[3]= 0x59aa76fe;
  N[4]= 0x5c37b001, N[5]= 0x60b44d1e, N[6]= 0x9a2ca556, N[7]= 0x12ab655e;

  threads = get_num_groups(0)*get_local_size(0);
  outer_times = 8192/threads;
  /*if (get_local_id(0)==0 && get_group_id(0)==0){
      printf("NTT_IO outer times is %d\n", outer_times);
      printf("NTT_IO total thread is %d\n", threads);
  }*/
  i = var_[0]; 
  stride = stride >> i;
  k = 1<<i;
  for (int I=0; I<outer_times; I++) {
   g_index = (I*threads+tid)/stride;
   index = (I*threads+tid)%stride;
   r = index + 2 * stride * g_index;

   for(int j=0; j<LEN_256; j++) {
     low[j] = var[r*LEN_256 + j];
     high[j] = var[r*LEN_256 + stride*LEN_256 + j];
     root[j] = g[index*k*LEN_256+j];
   }

   sub_assign_big_256(low, high, neg, temp, N);
   add_assign_big_256(low, high, low, N);
   mul_assign_big_256(root, neg, high, N);

   o_low = r;
   o_high = r + stride;
   /*       
   if (i == 13) {
     //bitverse
     M = 32 - Mbits;
     r_val = reverseBits(r)>>M;
     r_s_val = reverseBits(r + stride)>>M;
     o_low = r_val;
     o_high = r_s_val;
   }
   */
   for (int j=0; j<LEN_256; j++) {
     var[o_low*LEN_256 + j]= low[j];
     var[o_high*LEN_256 + j] = high[j];
   }
  } 
}

 /** 外部调用kernel bitreverse_NTT */
__kernel void bitreverse_NTT(__global uint *var) {
  uint tid = get_global_id(0);
  uint threads = get_num_groups(0)*get_local_size(0);
  uint outer_times = 16384/threads;
  uint index=0; 
  uint re_index = 0;
  uint M = 0;
  uint low[8], high[8];
  M = 32 - Mbits;
  for (int i=0; i<outer_times; i++) {
     index = i*threads + tid;
     re_index = reverseBits(index)>>M;
     if (index < re_index) {
        for (int j=0; j<LEN_256; j++) {
          low[j] = var[index*LEN_256 + j];
          high[j] = var[re_index*LEN_256 + j];
          var[re_index*LEN_256 + j]= low[j];
          var[index*LEN_256 + j] = high[j];
        } 
     }
  }
}

// 上层
 /** 外部调用kernel NTT_IO_projectivexyzz 第三个参数为线程共享变量 */
__kernel void NTT_IO_projectivexyzz(__global uint *var, __global ulong *g, __global uint* var_,__local uint *temp)
{
    // if(get_global_id(0) == 0){
    //     printf("var %04x rootg %lu",var[0],g[4]);
    // }
  uint tid = get_global_id(0);
  uint bid = get_local_id(0);
  ulong root[4];
  projective384xyzz a_;  // low
  projective384xyzz b;   // high
  projective384xyzz neg;
  uint stride = 8192;
  uint i;
  uint g_index;
  uint index;   
  uint r, k;
  uint M, r_val, r_s_val, o_low, o_high;
  uint threads = get_num_groups(0)*get_local_size(0);
  uint outer_times;
  
//   extern __shared__ uint temp[];
    // __local uint *temp;
  if (bid < 32) {
    for (int i=0; i<192; i++)
        temp[bid*192+i] = ModuleTable[i];
  }
//   __syncthreads();
  //
  barrier(CLK_LOCAL_MEM_FENCE);

  // GS
  outer_times = 8192/threads;
#if 1
  i = var_[0];
  /*
     if(get_local_id(0)==0 && get_group_id(0) ==0)
        printf("i is %d\n", i);
  */
  stride = stride >> i; 
  k = 1<<i;

  for (int I=0; I<outer_times; I++) {
    g_index =(I*threads + tid)/stride;   
    index = (I*threads + tid)%stride;
    r = index + 2 * stride * g_index;
  
    for(int j=0; j<LEN; j++) {
     a_.x[j] = var[r*4*LEN + j];
     a_.y[j] = var[r*4*LEN + j + LEN];
     a_.zz[j] = var[r*4*LEN + j + 2*LEN];
     a_.zzz[j] = var[r*4*LEN + j + 3*LEN];
     b.x[j] = var[r*4*LEN + stride*4*LEN + j];
     b.y[j] = var[r*4*LEN + stride*4*LEN + j + LEN];
     b.zz[j] = var[r*4*LEN + stride*4*LEN + j + 2*LEN];
     b.zzz[j] = var[r*4*LEN + stride*4*LEN + j + 3*LEN];
    }
    for (int j=0; j<4; j++) {
     root[j] = g[index*k*4+j];
    }

    projective384_negxyzz(&neg, &b);
    swj_add_assign_xyzz(&neg, &a_,temp);
    swj_add_assign_xyzz(&a_, &b,temp);
    projectivexyzzReduce(&a_,temp);
    swj_mul_projective_xyzz(&neg, root, 0, &b,temp);
    projectivexyzzReduce(&b,temp);   

    o_low = r;
    o_high = r + stride; 
/*    
  if (i == 13) {
    //bitverse
    M = 32 - Mbits;
    r_val = reverseBits(r)>>M;
    r_s_val = reverseBits(r + stride)>>M;
    o_low = r_val;
    o_high = r_s_val;     
  }
*/  

    for (int j=0; j<LEN; j++) {
     var[o_low*4*LEN + j]= a_.x[j];
     var[o_low*4*LEN + j + LEN] = a_.y[j];
     var[o_low*4*LEN + j + 2*LEN] = a_.zz[j];
     var[o_low*4*LEN + j + 3*LEN] = a_.zzz[j];
     var[o_high*4*LEN + j] = b.x[j];
     var[o_high*4*LEN + j + LEN] = b.y[j];
     var[o_high*4*LEN + j + 2*LEN] = b.zz[j];
     var[o_high*4*LEN + j + 3*LEN] = b.zzz[j];
    }
  } 
#endif
}

 /** 外部调用kernel bitreverse_NTT_xyzz */
__kernel void bitreverse_NTT_xyzz(__global uint *var) {
  uint tid = get_global_id(0);
  uint threads = get_num_groups(0)*get_local_size(0);
  uint outer_times = 16384/threads;
  uint index=0;
  uint re_index = 0;
  uint M = 0;
  projective384xyzz a;   // low
  projective384xyzz b;   // high
  M = 32 - Mbits;
  /*
  if (get_local_id(0)==0 && get_group_id(0)==0){
      printf("NTT_xyzz outer times is %d\n", outer_times);
      printf("NTT_xyzz total thread is %d\n", threads);
  }
  */
  for (int i=0; i<outer_times; i++) {
     index = i*threads + tid;
     re_index = reverseBits(index)>>M;
     if (index < re_index) {
        for (int j=0; j<LEN; j++) {
          a.x[j] = var[index*LEN*4 + j];
          a.y[j] = var[index*LEN*4 + j + LEN];
          a.zz[j] = var[index*LEN*4 + j + 2*LEN];
          a.zzz[j] = var[index*LEN*4 + j + 3*LEN];
          b.x[j] = var[re_index*LEN*4 + j];
          b.y[j] = var[re_index*LEN*4 + j + LEN];
          b.zz[j] = var[re_index*LEN*4 + j + 2*LEN];
          b.zzz[j] = var[re_index*LEN*4 + j + 3*LEN];
          var[index*LEN*4 + j] = b.x[j];
          var[index*LEN*4 + j + LEN] = b.y[j];
          var[index*LEN*4 + j + 2*LEN] = b.zz[j];
          var[index*LEN*4 + j + 3*LEN] = b.zzz[j];
          var[re_index*LEN*4 + j] = a.x[j];
          var[re_index*LEN*4 + j + LEN] = a.y[j];
          var[re_index*LEN*4 + j + 2*LEN] = a.zz[j];
          var[re_index*LEN*4 + j + 3*LEN] = a.zzz[j];
        } 
     }
  }
}

 /** 外部调用kernel repr256_to_projective384xyzz 第四个参数为线程共享变量 */
__kernel void repr256_to_projective384xyzz(__global ulong* var, __global uint * tempg, __global uint *r,__local uint *temp)  {
   
   uint tid = get_global_id(0);
   ulong e_p[4];
   ulong e_p_[4];
   projective384xyzz tempG, tempG_, r_G, r_G_;
   affine384 beta_g, beta_g_;
   uint temp_[12];
   uint outer_times;
   uint threads= get_num_groups(0)*get_local_size(0);
   
   if (get_local_id(0) < 32) {
    for (int i=0; i<192; i++){
        temp[get_local_id(0)*192+i] = ModuleTable[i];
        // if(i==191){
        //     printf("repr256_to_projective384xyzz temp: %u %u",temp[i],ModuleTable[i]);
        // }
    }
        
   }
   barrier(CLK_LOCAL_MEM_FENCE);

   outer_times = 8192/threads;
/*
   if (get_local_id(0)==0 && get_group_id(0)==0){
      printf("repr256_to_projective384xyzz outer times is %d\n", outer_times);
      printf("repr256_to_projective384xyzz total thread is %d\n", threads);
   }
*/ 
//    if(tid==0)
//    printf("outer_times %u threads %u LEN %u rlen%u",outer_times ,threads,LEN,sizeof(r));
   for (int i=0; i<outer_times; i++) {
     for (int j=0; j<4; j++) {
       e_p[j] = var[(i*threads+tid)*LEN_256 + j];
       e_p_[j] = var[(i*threads+tid)*LEN_256 + j + (LEN_256/2)];
     }
     for (int j=0; j<LEN; j++) {
       beta_g.x[j] = tempg[(i*threads+tid)*2*LEN*2 + j];
       beta_g.y[j] = tempg[(i*threads+tid)*2*LEN*2 + j + LEN];
       beta_g_.x[j] = tempg[(i*threads+tid)*2*LEN*2 + j + 2*LEN]; 
       beta_g_.y[j] = tempg[(i*threads+tid)*2*LEN*2 + j + 3*LEN];
     }
     big_to_repr_256(e_p);
     big_to_repr_256(e_p_);
     affine384_to_projective384_xyzz(&beta_g, &tempG);
     affine384_to_projective384_xyzz(&beta_g_, &tempG_);
     swj_mul_projective_xyzz(&tempG, e_p, 0,  &r_G,temp);
     swj_mul_projective_xyzz(&tempG_, e_p_, 0, &r_G_,temp);
     projectivexyzzReduce(&r_G,temp);
     projectivexyzzReduce(&r_G_,temp);
    
     for (int j=0; j<LEN; j++) {
        // if(tid==2) {
        //     printf("i:%u threads:%u tid:%u %u %u",i,threads,tid,((i*threads+tid)*2*LEN*4 + j),get_local_size(0));
        // }
       r[(i*threads+tid)*2*LEN*4 + j] = r_G.x[j];
       r[(i*threads+tid)*2*LEN*4 + j + LEN] = r_G.y[j];
       r[(i*threads+tid)*2*LEN*4 + j + 2*LEN] = r_G.zz[j]; 
       r[(i*threads+tid)*2*LEN*4 + j + 3*LEN] = r_G.zzz[j];
       r[(i*threads+tid)*2*LEN*4 + j + 4*LEN] = r_G_.x[j];
       r[(i*threads+tid)*2*LEN*4 + j + 5*LEN] = r_G_.y[j];
       r[(i*threads+tid)*2*LEN*4 + j + 6*LEN] = r_G_.zz[j];
       r[(i*threads+tid)*2*LEN*4 + j + 7*LEN] = r_G_.zzz[j];
     } 
   } 
}

/** ftt.cu end */

/** edwards_compute.cu start */
typedef struct {
  uint ed1_m_x[12];
  uint ed1_m_y[12];
  uint ed1_m_t[12];
  uint ed1_m_z[12];
  uint ed2_m_x[12];
  uint ed2_m_y[12];
  uint ed2_m_t[12];
  uint ed2_m_z[12];
  uint ed_m_x_d[12];
  uint ed_m_y_d[12];
  uint ed_m_t_d[12];
  uint ed_m_z_d[12];
  uint N[12]; 
  uint N2[12];
  uint N4[12];
  uint zero_384[12];
#ifdef testx
#else
  uint N3[12];
 // uint *N_p[4];
 //  = {zero_384, N, N2, N3};
#endif
  uint one[12];
  uint ED_COEFF_SQRT_NEG_A[12];
  uint mont_alpha[12];
  uint mont_beta[12];

} CurveXYZZ;

void __attribute__((overloadable)) CurveXYZZ0(CurveXYZZ *curveXYZZ) {
#ifdef testx    
    curveXYZZ->N[0]= 0x00000001, curveXYZZ->N[1]= 0x8508C000, curveXYZZ->N[2]= 0x30000000, curveXYZZ->N[3]= 0x170B5D44;
    curveXYZZ->N[4]= 0xBA094800, curveXYZZ->N[5]= 0x1EF3622F, curveXYZZ->N[6]= 0x00F5138F, curveXYZZ->N[7]= 0x1A22D9F3;
    curveXYZZ->N[8]= 0x6CA1493B, curveXYZZ->N[9]= 0xC63B05C0, curveXYZZ->N[10]= 0x17C510EA,curveXYZZ->N[11]= 0x01AE3A46;
    curveXYZZ->N2[0]= 0x00000002, curveXYZZ->N2[1]= 0x0A118000, curveXYZZ->N2[2]= 0x60000001, curveXYZZ->N2[3]= 0x2E16BA88;
    curveXYZZ->N2[4]= 0x74129000, curveXYZZ->N2[5]= 0x3DE6C45F, curveXYZZ->N2[6]= 0x01EA271E, curveXYZZ->N2[7]= 0x3445B3E6;
    curveXYZZ->N2[8]= 0xD9429276, curveXYZZ->N2[9]= 0x8C760B80,curveXYZZ->N2[10]= 0x2F8A21D5, curveXYZZ->N2[11]= 0x035C748C;
    curveXYZZ->N4[0]=0x00000004, curveXYZZ->N4[1]=0x14230000, curveXYZZ->N4[2]=0xC0000002, curveXYZZ->N4[3]=0x5C2D7510;
    curveXYZZ->N4[4]=0xE8252000, curveXYZZ->N4[5]=0x7BCD88BE, curveXYZZ->N4[6]=0x03D44E3C, curveXYZZ->N4[7]=0x688B67CC;
    curveXYZZ->N4[8]= 0xB28524EC, curveXYZZ->N4[9]= 0x18EC1701, curveXYZZ->N4[10]=0x5F1443AB, curveXYZZ->N4[11]=0x06B8E918;
    curveXYZZ->one[0] = 0xffffff68, curveXYZZ->one[1] = 0x02cdffff, curveXYZZ->one[2] = 0x7fffffb1;
    curveXYZZ->one[3] = 0x51409f83, curveXYZZ->one[4] = 0x8a7d3ff2, curveXYZZ->one[5] = 0x9f7db3a9;
    curveXYZZ->one[6] = 0x6e7c6305, curveXYZZ->one[7] = 0x7b4e97b7, curveXYZZ->one[8] = 0x803c84e8;
    curveXYZZ->one[9] = 0x4cf495bf, curveXYZZ->one[10] = 0xe2fdf49a, curveXYZZ->one[11] = 0x008d6661; 
    curveXYZZ->zero_384[0] = 0x00000000, curveXYZZ->zero_384[1]=0x00000000, curveXYZZ->zero_384[2]=0x00000000;
    curveXYZZ->zero_384[3] = 0x00000000, curveXYZZ->zero_384[4]=0x00000000, curveXYZZ->zero_384[5]=0x00000000;
    curveXYZZ->zero_384[6] = 0x00000000, curveXYZZ->zero_384[7]=0x00000000, curveXYZZ->zero_384[8]=0x00000000;
    curveXYZZ->zero_384[9] = 0x00000000, curveXYZZ->zero_384[10]=0x00000000, curveXYZZ->zero_384[11]=0x00000000;
#else
    curveXYZZ->N[0]= 0x00000001, curveXYZZ->N[1]= 0x8508C000, curveXYZZ->N[2]= 0x30000000, curveXYZZ->N[3]= 0x170B5D44;
    curveXYZZ->N[4]= 0xBA094800, curveXYZZ->N[5]= 0x1EF3622F, curveXYZZ->N[6]= 0x00F5138F, curveXYZZ->N[7]= 0x1A22D9F3;
    curveXYZZ->N[8]= 0x6CA1493B, curveXYZZ->N[9]= 0xC63B05C0, curveXYZZ->N[10]= 0x17C510EA,curveXYZZ->N[11]= 0x01AE3A46;
    curveXYZZ->N2[0]= 0x00000002, curveXYZZ->N2[1]= 0x0A118000, curveXYZZ->N2[2]= 0x60000001, curveXYZZ->N2[3]= 0x2E16BA88;
    curveXYZZ->N2[4]= 0x74129000, curveXYZZ->N2[5]= 0x3DE6C45F, curveXYZZ->N2[6]= 0x01EA271E, curveXYZZ->N2[7]= 0x3445B3E6;
    curveXYZZ->N2[8]= 0xD9429276, curveXYZZ->N2[9]= 0x8C760B80,curveXYZZ->N2[10]= 0x2F8A21D5, curveXYZZ->N2[11]= 0x035C748C;
    curveXYZZ->N3[0]= 0x00000003, curveXYZZ->N3[1]= 0x8F1A4000, curveXYZZ->N3[2]= 0x90000001, curveXYZZ->N3[3]= 0x452217CC;
    curveXYZZ->N3[4]= 0x2E1BD800, curveXYZZ->N3[5]= 0x5CDA268F, curveXYZZ->N3[6]= 0x02DF3AAD, curveXYZZ->N3[7]= 0x4E688DD9;
    curveXYZZ->N3[8]= 0x45E3DBB1, curveXYZZ->N3[9]= 0x52B11141,curveXYZZ->N3[10]= 0x474F32C0, curveXYZZ->N3[11]= 0x050AAED2;
    curveXYZZ->N4[0]=0x00000004, curveXYZZ->N4[1]=0x14230000, curveXYZZ->N4[2]=0xC0000002, curveXYZZ->N4[3]=0x5C2D7510;
    curveXYZZ->N4[4]=0xE8252000, curveXYZZ->N4[5]=0x7BCD88BE, curveXYZZ->N4[6]=0x03D44E3C, curveXYZZ->N4[7]=0x688B67CC;
    curveXYZZ->N4[8]= 0xB28524EC, curveXYZZ->N4[9]= 0x18EC1701, curveXYZZ->N4[10]=0x5F1443AB, curveXYZZ->N4[11]=0x06B8E918;
    curveXYZZ->zero_384[0] = 0x00000000, curveXYZZ->zero_384[1]=0x00000000, curveXYZZ->zero_384[2]=0x00000000;
    curveXYZZ->zero_384[3] = 0x00000000, curveXYZZ->zero_384[4]=0x00000000, curveXYZZ->zero_384[5]=0x00000000;
    curveXYZZ->zero_384[6] = 0x00000000, curveXYZZ->zero_384[7]=0x00000000, curveXYZZ->zero_384[8]=0x00000000;
    curveXYZZ->zero_384[9] = 0x00000000, curveXYZZ->zero_384[10]=0x00000000, curveXYZZ->zero_384[11]=0x00000000;    
    curveXYZZ->one[0] = 0xffffff68, curveXYZZ->one[1] = 0x02cdffff, curveXYZZ->one[2] = 0x7fffffb1;
    curveXYZZ->one[3] = 0x51409f83, curveXYZZ->one[4] = 0x8a7d3ff2, curveXYZZ->one[5] = 0x9f7db3a9;
    curveXYZZ->one[6] = 0x6e7c6305, curveXYZZ->one[7] = 0x7b4e97b7, curveXYZZ->one[8] = 0x803c84e8;
    curveXYZZ->one[9] = 0x4cf495bf, curveXYZZ->one[10] = 0xe2fdf49a, curveXYZZ->one[11] = 0x008d6661;         
#endif
  }

void __attribute__((overloadable)) CurveXYZZ1(bool convert,CurveXYZZ *curveXYZZ) {
    curveXYZZ->N[0]= 0x00000001, curveXYZZ->N[1]= 0x8508C000, curveXYZZ->N[2]= 0x30000000, curveXYZZ->N[3]= 0x170B5D44;
    curveXYZZ->N[4]= 0xBA094800, curveXYZZ->N[5]= 0x1EF3622F, curveXYZZ->N[6]= 0x00F5138F, curveXYZZ->N[7]= 0x1A22D9F3;
    curveXYZZ->N[8]= 0x6CA1493B, curveXYZZ->N[9]= 0xC63B05C0, curveXYZZ->N[10]= 0x17C510EA,curveXYZZ->N[11]= 0x01AE3A46;
    curveXYZZ->zero_384[0] = 0x00000000, curveXYZZ->zero_384[1]=0x00000000, curveXYZZ->zero_384[2]=0x00000000;
    curveXYZZ->zero_384[3] = 0x00000000, curveXYZZ->zero_384[4]=0x00000000, curveXYZZ->zero_384[5]=0x00000000;
    curveXYZZ->zero_384[6] = 0x00000000, curveXYZZ->zero_384[7]=0x00000000, curveXYZZ->zero_384[8]=0x00000000;
    curveXYZZ->zero_384[9] = 0x00000000, curveXYZZ->zero_384[10]=0x00000000, curveXYZZ->zero_384[11]=0x00000000;
    curveXYZZ->one[0] = 0xffffff68, curveXYZZ->one[1] = 0x02cdffff, curveXYZZ->one[2] = 0x7fffffb1;
    curveXYZZ->one[3] = 0x51409f83, curveXYZZ->one[4] = 0x8a7d3ff2, curveXYZZ->one[5] = 0x9f7db3a9;
    curveXYZZ->one[6] = 0x6e7c6305, curveXYZZ->one[7] = 0x7b4e97b7, curveXYZZ->one[8] = 0x803c84e8;
    curveXYZZ->one[9] = 0x4cf495bf, curveXYZZ->one[10] = 0xe2fdf49a, curveXYZZ->one[11] = 0x008d6661; 
    curveXYZZ->ED_COEFF_SQRT_NEG_A[0] =0xd41bc23d, curveXYZZ->ED_COEFF_SQRT_NEG_A[1] = 0x5945fd4f;
    curveXYZZ->ED_COEFF_SQRT_NEG_A[2] =0x6559b338, curveXYZZ->ED_COEFF_SQRT_NEG_A[3] = 0xb11cec70;
    curveXYZZ->ED_COEFF_SQRT_NEG_A[4] =0x722fdb39, curveXYZZ->ED_COEFF_SQRT_NEG_A[5] = 0x853e6bcf;
    curveXYZZ->ED_COEFF_SQRT_NEG_A[6] =0xa887936b, curveXYZZ->ED_COEFF_SQRT_NEG_A[7] = 0x53fa5979;
    curveXYZZ->ED_COEFF_SQRT_NEG_A[8] =0x1018491a, curveXYZZ->ED_COEFF_SQRT_NEG_A[9] = 0xfe77e873;
    curveXYZZ->ED_COEFF_SQRT_NEG_A[10] = 0xb8c57a82, curveXYZZ->ED_COEFF_SQRT_NEG_A[11] =0xdee4cd;
    curveXYZZ->mont_beta[0]= 0x8b83be45,curveXYZZ->mont_beta[1]=0x0320f221,curveXYZZ->mont_beta[2]=0x899abb8a;
    curveXYZZ->mont_beta[3]= 0x8c9ecfc2,curveXYZZ->mont_beta[4]=0xf494c488,curveXYZZ->mont_beta[5]=0x84b44205;
    curveXYZZ->mont_beta[6]= 0x336eb9e7,curveXYZZ->mont_beta[7]=0xc1a6e49a,curveXYZZ->mont_beta[8]=0xe2b475ca;
    curveXYZZ->mont_beta[9]= 0xa93aec49,curveXYZZ->mont_beta[10]=0x161de008,curveXYZZ->mont_beta[11]=0x01295b55;
    curveXYZZ->mont_alpha[0]= 0xa58478da,curveXYZZ->mont_alpha[1]=0x5892506d,curveXYZZ->mont_alpha[2]= 0x0ac2a74b;
    curveXYZZ->mont_alpha[3]= 0x13336694,curveXYZZ->mont_alpha[4]=0xcdf726cf,curveXYZZ->mont_alpha[5]= 0x9b64a150;
    curveXYZZ->mont_alpha[6]= 0x0a9c587e,curveXYZZ->mont_alpha[7]=0x5cc42609,curveXYZZ->mont_alpha[8]= 0xfdcd640c;
    curveXYZZ->mont_alpha[9]= 0x5cf848ad,curveXYZZ->mont_alpha[10]=0x3ac02380,curveXYZZ->mont_alpha[11]=0x004702bf;
  }

    void cpy(CurveXYZZ *curveXYZZ){
     #pragma unroll
     for (int i=0; i<12; i++) {
        curveXYZZ->ed_m_x_d[i] = curveXYZZ->ed1_m_x[i];
        curveXYZZ->ed_m_y_d[i] = curveXYZZ->ed1_m_y[i];
        curveXYZZ->ed_m_t_d[i] = curveXYZZ->ed1_m_t[i];
        curveXYZZ->ed_m_z_d[i] = curveXYZZ->ed1_m_z[i];
     }
  }

    void settypezero_xyt(CurveXYZZ *curveXYZZ){
    curveXYZZ->ed1_m_x[0] = 0x00, curveXYZZ->ed1_m_x[1] = 0x00, curveXYZZ->ed1_m_x[2] = 0x00;
    curveXYZZ->ed1_m_x[3] = 0x00, curveXYZZ->ed1_m_x[4] = 0x00, curveXYZZ->ed1_m_x[5] = 0x00;
    curveXYZZ->ed1_m_x[6] = 0x00, curveXYZZ->ed1_m_x[7] = 0x00, curveXYZZ->ed1_m_x[8] = 0x00;
    curveXYZZ->ed1_m_x[9] = 0x00, curveXYZZ->ed1_m_x[10] = 0x00, curveXYZZ->ed1_m_x[11] = 0x00;
    curveXYZZ->ed1_m_y[0] = 0xffffff68, curveXYZZ->ed1_m_y[1] = 0x02cdffff, curveXYZZ->ed1_m_y[2] = 0x7fffffb1;
    curveXYZZ->ed1_m_y[3] = 0x51409f83, curveXYZZ->ed1_m_y[4] = 0x8a7d3ff2, curveXYZZ->ed1_m_y[5] = 0x9f7db3a9;
    curveXYZZ->ed1_m_y[6] = 0x6e7c6305, curveXYZZ->ed1_m_y[7] = 0x7b4e97b7, curveXYZZ->ed1_m_y[8] = 0x803c84e8;
    curveXYZZ->ed1_m_y[9] = 0x4cf495bf, curveXYZZ->ed1_m_y[10] = 0xe2fdf49a, curveXYZZ->ed1_m_y[11] = 0x008d6661;
    curveXYZZ->ed1_m_t[0] = 0x00, curveXYZZ->ed1_m_t[1] = 0x00, curveXYZZ->ed1_m_t[2] = 0x00;
    curveXYZZ->ed1_m_t[3] = 0x00, curveXYZZ->ed1_m_t[4] = 0x00, curveXYZZ->ed1_m_t[5] = 0x00;
    curveXYZZ->ed1_m_t[6] = 0x00, curveXYZZ->ed1_m_t[7] = 0x00, curveXYZZ->ed1_m_t[8] = 0x00;
    curveXYZZ->ed1_m_t[9] = 0x00, curveXYZZ->ed1_m_t[10] = 0x00, curveXYZZ->ed1_m_t[11] = 0x00;
  }

    void settypezero(CurveXYZZ *curveXYZZ){
    curveXYZZ->ed1_m_x[0] = 0x00, curveXYZZ->ed1_m_x[1] = 0x00, curveXYZZ->ed1_m_x[2] = 0x00;
    curveXYZZ->ed1_m_x[3] = 0x00, curveXYZZ->ed1_m_x[4] = 0x00, curveXYZZ->ed1_m_x[5] = 0x00;
    curveXYZZ->ed1_m_x[6] = 0x00, curveXYZZ->ed1_m_x[7] = 0x00, curveXYZZ->ed1_m_x[8] = 0x00;
    curveXYZZ->ed1_m_x[9] = 0x00, curveXYZZ->ed1_m_x[10] = 0x00, curveXYZZ->ed1_m_x[11] = 0x00;
    curveXYZZ->ed1_m_y[0] = 0xffffff68, curveXYZZ->ed1_m_y[1] = 0x02cdffff, curveXYZZ->ed1_m_y[2] = 0x7fffffb1;
    curveXYZZ->ed1_m_y[3] = 0x51409f83, curveXYZZ->ed1_m_y[4] = 0x8a7d3ff2, curveXYZZ->ed1_m_y[5] = 0x9f7db3a9;
    curveXYZZ->ed1_m_y[6] = 0x6e7c6305, curveXYZZ->ed1_m_y[7] = 0x7b4e97b7, curveXYZZ->ed1_m_y[8] = 0x803c84e8;
    curveXYZZ->ed1_m_y[9] = 0x4cf495bf, curveXYZZ->ed1_m_y[10] = 0xe2fdf49a, curveXYZZ->ed1_m_y[11] = 0x008d6661;
    curveXYZZ->ed1_m_t[0] = 0x00, curveXYZZ->ed1_m_t[1] = 0x00, curveXYZZ->ed1_m_t[2] = 0x00;
    curveXYZZ->ed1_m_t[3] = 0x00, curveXYZZ->ed1_m_t[4] = 0x00, curveXYZZ->ed1_m_t[5] = 0x00;
    curveXYZZ->ed1_m_t[6] = 0x00, curveXYZZ->ed1_m_t[7] = 0x00, curveXYZZ->ed1_m_t[8] = 0x00;
    curveXYZZ->ed1_m_t[9] = 0x00, curveXYZZ->ed1_m_t[10] = 0x00, curveXYZZ->ed1_m_t[11] = 0x00;
    curveXYZZ->ed1_m_z[0] = 0xffffff68, curveXYZZ->ed1_m_z[1] = 0x02cdffff, curveXYZZ->ed1_m_z[2] = 0x7fffffb1;
    curveXYZZ->ed1_m_z[3] = 0x51409f83, curveXYZZ->ed1_m_z[4] = 0x8a7d3ff2, curveXYZZ->ed1_m_z[5] = 0x9f7db3a9;
    curveXYZZ->ed1_m_z[6] = 0x6e7c6305, curveXYZZ->ed1_m_z[7] = 0x7b4e97b7, curveXYZZ->ed1_m_z[8] =  0x803c84e8;
    curveXYZZ->ed1_m_z[9] = 0x4cf495bf, curveXYZZ->ed1_m_z[10] = 0xe2fdf49a, curveXYZZ->ed1_m_z[11] = 0x008d6661;
  } 


     void  XYZZReduce(CurveXYZZ *curveXYZZ,__local uint *sharedTemp) {
     uint temp[12];
     xyzzReduce(curveXYZZ->ed_m_x_d, curveXYZZ->ed_m_x_d, temp,sharedTemp);
     xyzzReduce(curveXYZZ->ed_m_y_d, curveXYZZ->ed_m_y_d, temp,sharedTemp);
     xyzzReduce(curveXYZZ->ed_m_t_d, curveXYZZ->ed_m_t_d, temp,sharedTemp);
     xyzzReduce(curveXYZZ->ed_m_z_d, curveXYZZ->ed_m_z_d, temp,sharedTemp);
  }

     void  XYZZReduce_(CurveXYZZ *curveXYZZ,__local uint *sharedTemp) {
     uint temp[12];
     xyzzReduce_(curveXYZZ->ed_m_x_d, curveXYZZ->ed_m_x_d, temp,sharedTemp);
     xyzzReduce_(curveXYZZ->ed_m_y_d, curveXYZZ->ed_m_y_d, temp,sharedTemp);
     xyzzReduce_(curveXYZZ->ed_m_t_d, curveXYZZ->ed_m_t_d, temp,sharedTemp);
     xyzzReduce_(curveXYZZ->ed_m_z_d, curveXYZZ->ed_m_z_d, temp,sharedTemp);
  }


    void neg_384(CurveXYZZ *curveXYZZ) {
     if (!is_zero_big_384(curveXYZZ->ed_m_x_d)) {
        sub_big_384(curveXYZZ->N, curveXYZZ->ed_m_x_d, curveXYZZ->ed_m_x_d);
     }
     if (!is_zero_big_384(curveXYZZ->ed_m_t_d)) {
        sub_big_384(curveXYZZ->N, curveXYZZ->ed_m_t_d, curveXYZZ->ed_m_t_d);
     }
  }

    void edwardsproj_to_edwardsaffine_big384(CurveXYZZ *curveXYZZ) {
      uint temp[12];     
      if(is_zero_big_384(curveXYZZ->ed_m_z_d)) {
           cpy_big_384(curveXYZZ->ed_m_x_d, curveXYZZ->zero_384);
           cpy_big_384(curveXYZZ->ed_m_y_d, curveXYZZ->one);
           cpy_big_384(curveXYZZ->ed_m_t_d, curveXYZZ->zero_384);  
      }
      inverse_big_384(temp, curveXYZZ->ed_m_z_d, curveXYZZ->N);
      mul_assign_big_384(curveXYZZ->ed_m_x_d, temp, curveXYZZ->ed_m_x_d, curveXYZZ->N);
      mul_assign_big_384(curveXYZZ->ed_m_y_d, temp, curveXYZZ->ed_m_y_d, curveXYZZ->N);
      mul_assign_big_384(curveXYZZ->ed_m_x_d, curveXYZZ->ed_m_y_d, curveXYZZ->ed_m_t_d, curveXYZZ->N);
  }

    void edwardsproj_to_edwardsaffine_big384_(CurveXYZZ *curveXYZZ) {
      uint temp[12], temp_1[12], temp_2[12];
      if(is_zero_big_384(curveXYZZ->ed_m_z_d)) {
           cpy_big_384(curveXYZZ->ed_m_x_d, curveXYZZ->zero_384);
           cpy_big_384(curveXYZZ->ed_m_y_d, curveXYZZ->one);
           cpy_big_384(curveXYZZ->ed_m_t_d, curveXYZZ->zero_384);  
      }
      inverse_big_384(temp, curveXYZZ->ed_m_z_d, curveXYZZ->N);
      mul_assign_big_384(curveXYZZ->ed_m_x_d, temp, curveXYZZ->ed_m_x_d, curveXYZZ->N);
      mul_assign_big_384(curveXYZZ->ed_m_y_d, temp, curveXYZZ->ed_m_y_d, curveXYZZ->N);
      add_big_384(curveXYZZ->ed_m_y_d, curveXYZZ->ed_m_x_d, temp_1);
      sub_big_384(curveXYZZ->ed_m_y_d, curveXYZZ->ed_m_x_d, temp_2);
      add_big_384(temp_2, curveXYZZ->N, temp_2);
      mul_assign_big_384(curveXYZZ->ed_m_x_d, curveXYZZ->ed_m_y_d, curveXYZZ->ed_m_t_d, curveXYZZ->N);
      cpy_big_384(curveXYZZ->ed_m_x_d, temp_1);
      cpy_big_384(curveXYZZ->ed_m_y_d, temp_2);
      double_big_384(curveXYZZ->ed_m_t_d, curveXYZZ->ed_m_t_d);
  }

    void just_use(CurveXYZZ *curveXYZZ) {
      uint temp_1[12], temp_2[12]; 
      add_big_384(curveXYZZ->ed_m_y_d, curveXYZZ->ed_m_x_d, temp_1);
      sub_big_384(curveXYZZ->ed_m_y_d, curveXYZZ->ed_m_x_d, temp_2);
      add_big_384(temp_2, curveXYZZ->N, temp_2);
      cpy_big_384(curveXYZZ->ed_m_x_d, temp_1);
      cpy_big_384(curveXYZZ->ed_m_y_d, temp_2);
      double_big_384(curveXYZZ->ed_m_t_d, curveXYZZ->ed_m_t_d);
  }
  
    void big384_from_neg_one_a(CurveXYZZ *curveXYZZ) {
    uint temp[12];    
    mul_assign_big_384(curveXYZZ->ed_m_x_d, curveXYZZ->ED_COEFF_SQRT_NEG_A, temp, curveXYZZ->N);
    mul_assign_big_384(temp, curveXYZZ->ed_m_y_d, curveXYZZ->ed_m_t_d, curveXYZZ->N);
    mul_assign_big_384(curveXYZZ->ED_COEFF_SQRT_NEG_A, curveXYZZ->ed_m_x_d, curveXYZZ->ed_m_x_d, curveXYZZ->N);
  }
  
    void big384_to_neg_one_a(CurveXYZZ *curveXYZZ) {
    uint ED_COEFF_SQRT_NEG_A[12];
    uint t[12];
    ED_COEFF_SQRT_NEG_A[0] =0xf6255d75, ED_COEFF_SQRT_NEG_A[1] = 0x90946f8d;
    ED_COEFF_SQRT_NEG_A[2] =0x9531916c, ED_COEFF_SQRT_NEG_A[3] = 0x74b2abaf;
    ED_COEFF_SQRT_NEG_A[4] =0x1f381c4b, ED_COEFF_SQRT_NEG_A[5] = 0x983c3c76;
    ED_COEFF_SQRT_NEG_A[6] =0x8ce958ab, ED_COEFF_SQRT_NEG_A[7] = 0x50efb8d3;
    ED_COEFF_SQRT_NEG_A[8] =0x91b3dd54, ED_COEFF_SQRT_NEG_A[9] = 0x10f5ee88;
    ED_COEFF_SQRT_NEG_A[10] = 0xf0a0b908, ED_COEFF_SQRT_NEG_A[11] =0x01167ec8;
    mul_assign_big_384(curveXYZZ->ed1_m_x,ED_COEFF_SQRT_NEG_A,t,curveXYZZ->N);
    mul_assign_big_384(t,curveXYZZ->ed1_m_y,curveXYZZ->ed1_m_t,curveXYZZ->N);
    mul_assign_big_384(ED_COEFF_SQRT_NEG_A,curveXYZZ->ed1_m_x,curveXYZZ->ed1_m_x,curveXYZZ->N);
  }

   void edwardsaffine_to_swaffine_big384(CurveXYZZ *curveXYZZ) {
    uint temp[12], temp1[12], temp_[12];
    uint mont_x[12], mont_y[12];
    if (is_zero_big_384(curveXYZZ->ed_m_x_d) &&
       is_one_big_384(curveXYZZ->ed_m_y_d)) {    
       cpy_big_384(curveXYZZ->ed_m_x_d, curveXYZZ->zero_384);
       cpy_big_384(curveXYZZ->ed_m_y_d, curveXYZZ->zero_384);
       return;
    }

 	  sub_assign_big_384(curveXYZZ->one, curveXYZZ->ed_m_y_d, temp, temp_, curveXYZZ->N);
	  inverse_big_384(temp, temp, curveXYZZ->N); 
 	  add_assign_big_384(curveXYZZ->one, curveXYZZ->ed_m_y_d, temp1, curveXYZZ->N);
	  mul_assign_big_384(temp1, temp, mont_x, curveXYZZ->N);

    mul_assign_big_384(curveXYZZ->ed_m_x_d, curveXYZZ->ed_m_y_d, temp, curveXYZZ->N);
	  sub_assign_big_384(curveXYZZ->ed_m_x_d, temp, temp, temp_, curveXYZZ->N);
	  inverse_big_384(temp, temp, curveXYZZ->N);
	  mul_assign_big_384(temp1, temp, mont_y, curveXYZZ->N);

    mul_assign_big_384(mont_x, curveXYZZ->mont_beta,temp, curveXYZZ->N);
    add_assign_big_384(temp, curveXYZZ->mont_alpha,curveXYZZ->ed_m_x_d, curveXYZZ->N);
    mul_assign_big_384(mont_y,curveXYZZ->mont_beta,curveXYZZ->ed_m_y_d, curveXYZZ->N);

  }

    void swaffine_to_edwardsaffine_big384(CurveXYZZ *curveXYZZ) {
   uint mont_x[12],mont_y[12],temp1[12],betaInv[12];
   uint temp_[12], temp2[12];

   if (is_zero_big_384(curveXYZZ->ed1_m_x)&&
       is_zero_big_384(curveXYZZ->ed1_m_y)) {       
        mp_copy(LEN,curveXYZZ->ed1_m_x, curveXYZZ->zero_384);
        mp_copy(LEN,curveXYZZ->ed1_m_y, curveXYZZ->one);
        mp_copy(LEN,curveXYZZ->ed1_m_t, curveXYZZ->zero_384);
        return;
   }
   inverse_big_384(betaInv, curveXYZZ->mont_beta, curveXYZZ->N);
   sub_assign_big_384(curveXYZZ->ed1_m_x, curveXYZZ->mont_alpha, mont_x, temp2, curveXYZZ->N);
   mul_assign_big_384(mont_x, betaInv, mont_x, curveXYZZ->N);
   mul_assign_big_384(curveXYZZ->ed1_m_y,betaInv, mont_y, curveXYZZ->N);
   add_assign_big_384(mont_x, curveXYZZ->one, temp1, curveXYZZ->N);

   if (is_zero_big_384(mont_y)&&
       is_zero_big_384(temp1)) {
       mp_copy(LEN,curveXYZZ->ed1_m_x, curveXYZZ->zero_384);
       mp_copy(LEN,curveXYZZ->ed1_m_y, curveXYZZ->one);
       mp_copy(LEN,curveXYZZ->ed1_m_t, curveXYZZ->zero_384);
       return;
    }   
    inverse_big_384(mont_y, mont_y, curveXYZZ->N);
    mul_assign_big_384(mont_x, mont_y, curveXYZZ->ed1_m_x, curveXYZZ->N);
    add_assign_big_384(mont_x, curveXYZZ->one, temp_, curveXYZZ->N);
    sub_assign_big_384(mont_x, curveXYZZ->one, temp1, temp2, curveXYZZ->N);
    inverse_big_384(temp_, temp_, curveXYZZ->N);
    mul_assign_big_384(temp_, temp1, curveXYZZ->ed1_m_y, curveXYZZ->N);
    mul_assign_big_384(curveXYZZ->ed1_m_x, curveXYZZ->ed1_m_y, curveXYZZ->ed1_m_t, curveXYZZ->N);
  }

    void curve_affine384_to_projective384_xyzz(CurveXYZZ *curveXYZZ)
  {
    if (is_zero_big_384(curveXYZZ->ed1_m_x)&&is_one_big_384(curveXYZZ->ed1_m_y)) {
      for (int i=0; i<12; i++) {     
        curveXYZZ->ed_m_x_d[i]= curveXYZZ->zero_384[i];
        curveXYZZ->ed_m_y_d[i]= curveXYZZ->one[i];
        curveXYZZ->ed_m_t_d[i]= curveXYZZ->zero_384[i];
        curveXYZZ->ed_m_z_d[i]= curveXYZZ->zero_384[i];
      }  
    } else {  
      for (int i=0; i<12; i++) {
        curveXYZZ->ed_m_x_d[i]= curveXYZZ->ed1_m_x[i];
        curveXYZZ->ed_m_y_d[i]= curveXYZZ->ed1_m_y[i];
        curveXYZZ->ed_m_t_d[i]= curveXYZZ->one[i];
        curveXYZZ->ed_m_z_d[i]= curveXYZZ->one[i];
      }
    }
  }

    void projective384xyzz_to_affine384(CurveXYZZ *curveXYZZ)
  {
    uint T0[12], T1[12], temp[12];
    if (is_zero_big_384(curveXYZZ->ed_m_t_d))
    {
        for (int i=0; i<12; i++) {
           curveXYZZ->ed1_m_x[i] = curveXYZZ->zero_384[i];
           curveXYZZ->ed1_m_y[i] = curveXYZZ->one[i];
        } 
    } else {
        // Z is nonzero, so it must have an inverse in a field.      
        inverse_big_384(T0, curveXYZZ->ed_m_z_d, curveXYZZ->N);
        mul_assign_big_384(curveXYZZ->ed_m_y_d, T0, curveXYZZ->ed1_m_y, curveXYZZ->N);
        mul_assign_big_384(curveXYZZ->ed_m_t_d, T0, T0, curveXYZZ->N);
        sqr_assign_big_384(temp, T0, T1, curveXYZZ->N);
        mul_assign_big_384(curveXYZZ->ed_m_x_d, T1, curveXYZZ->ed1_m_x, curveXYZZ->N);
    }
  }
  /** edwards_compute.cu end */


  /** edwards_curve.cu start */

  //need ajust
  #define MODULE_TABLE_SIZE 192*32*4 

 /** 外部调用kernel edwardsproj_to_swaffine384 */
 __kernel void  edwardsproj_to_swaffine384(__global uint *var, __global uint* r) {
   uint tid = get_local_id(0);
   uint bid = get_group_id(0);
   uint threads = core_per_sm;
   uint commitment_x_flag;
   uint negY[12];
   uint tempY[12];
   CurveXYZZ curve;
   CurveXYZZ1(1,&curve);
   for (int i=0; i<LEN; i++) {
       (curve.ed_m_x_d[i]) = var[bid*threads*48+tid*48+i];
       (curve.ed_m_y_d[i]) = var[bid*threads*48+tid*48+i+LEN];
       (curve.ed_m_t_d[i]) = var[bid*threads*48+tid*48+i+2*LEN];
       (curve.ed_m_z_d[i]) = var[bid*threads*48+tid*48+i+3*LEN]; 
   }
   edwardsproj_to_edwardsaffine_big384(&curve);
   big384_from_neg_one_a(&curve);
   edwardsaffine_to_swaffine_big384(&curve);

   big_to_repr_384(curve.ed_m_x_d, curve.ed_m_x_d);
   big_to_repr_384(tempY, curve.ed_m_y_d); //temp_y

   if (!is_zero_big_384(curve.ed_m_y_d)) {
      sub_big_384(curve.N, curve.ed_m_y_d, negY);
   }

   big_to_repr_384(negY, negY);
   commitment_x_flag = 0x00000000;
   if(cmp_big_384(tempY, negY)==1) {
      commitment_x_flag = 0x80000000;
   }
   for (int i=0; i<LEN; i++) {
       r[bid*threads*37+tid*37+i] = (curve.ed_m_x_d[i]);
       r[bid*threads*37+tid*37+i+LEN] = (tempY[i]);
   }
   for (int i=0; i<LEN-1; i++) {
       r[bid*threads*37+tid*37+i+2*LEN] = (curve.ed_m_x_d[i]);
   }
   r[bid*threads*37+tid*37+LEN-1+2*LEN] = (curve.ed_m_x_d[11])|commitment_x_flag;
   r[bid*threads*37+tid*37+3*LEN] = commitment_x_flag>>24;
}

 /** 外部调用kernel projectxyzz_to_neg_one 第三个参数为线程共享变量*/
__kernel void projectxyzz_to_neg_one(__global uint *Geval, __global uint* Geval5tw,__local uint *temp) {
   uint tid = get_global_id(0);
   uint threads = get_local_size(0)*get_num_groups(0);
   uint outer_times = 0;
#if USE_VAR==1
   uint evaluation_size = device_evaluation_size;
#endif      
   if (get_local_id(0) < 32) {
        for (int i=0; i<192; i++)
          temp[get_local_id(0)*192+i] = ModuleTable[i];
   }
   barrier(CLK_LOCAL_MEM_FENCE);

   CurveXYZZ  curve;
   CurveXYZZ1(1,&curve);
   outer_times = 8192/threads;

   for (int i=0; i<outer_times; i++) {
      for (int j=0; j<LEN; j++) {
       curve.ed_m_x_d[j] = Geval[(i*threads + tid)*4*LEN + j];
       curve.ed_m_y_d[j] = Geval[(i*threads + tid)*4*LEN + j + LEN];
       curve.ed_m_t_d[j] = Geval[(i*threads + tid)*4*LEN + j + 2*LEN];
       curve.ed_m_z_d[j] = Geval[(i*threads + tid)*4*LEN + j + 3*LEN];
      }
      XYZZReduce_(&curve,temp);
      projective384xyzz_to_affine384(&curve);
      swaffine_to_edwardsaffine_big384(&curve);
      big384_to_neg_one_a(&curve);

      for (int j=0; j<LEN; j++) {
       Geval5tw[(i*threads+tid)*3*LEN + j] = curve.ed1_m_x[j];
       Geval5tw[(i*threads+tid)*3*LEN + j + LEN] = curve.ed1_m_y[j];
       Geval5tw[(i*threads+tid)*3*LEN + j + 2*LEN] = curve.ed1_m_t[j];
      }   
   }
#if USE_VAR==1
   if (tid<(evaluation_size - 8192)) {
#else
   if (tid<(Evaluations_size_ - 8192)) {
#endif    
     settypezero_xyt(&curve);
     for (int i=0; i<LEN; i++) {
       Geval5tw[(8192+tid)*3*LEN + i] = curve.ed1_m_x[i];
       Geval5tw[(8192+tid)*3*LEN + i + LEN] = curve.ed1_m_y[i];
       Geval5tw[(8192+tid)*3*LEN + i + 2*LEN] = curve.ed1_m_t[i];
     }
   }
}



/**sha256.cu start */

#define SHA256_BLOCK_SIZE 32 // SHA256 outputs a 32 byte digest

#define ROTLEFT(a, b) (((a) << (b)) | ((a) >> (32 - (b))))
#define ROTRIGHT(a, b) (((a) >> (b)) | ((a) << (32 - (b))))

#define CH(x, y, z) (((x) & (y)) ^ (~(x) & (z)))
#define MAJ(x, y, z) (((x) & (y)) ^ ((x) & (z)) ^ ((y) & (z)))
#define EP0(x) (ROTRIGHT(x, 2) ^ ROTRIGHT(x, 13) ^ ROTRIGHT(x, 22))
#define EP1(x) (ROTRIGHT(x, 6) ^ ROTRIGHT(x, 11) ^ ROTRIGHT(x, 25))
#define SIG0(x) (ROTRIGHT(x, 7) ^ ROTRIGHT(x, 18) ^ ((x) >> 3))
#define SIG1(x) (ROTRIGHT(x, 17) ^ ROTRIGHT(x, 19) ^ ((x) >> 10))

// typedef unsigned char BYTE; // 8-bit byte
// typedef uint WORD;      // 32-bit word, change to "long" for 16-bit machines

typedef struct {
  uchar data[64];
  uint datalen;
  unsigned long long bitlen;
  uint state[8];
} SHA256_CTX;

 __constant uint dev_k[64] = {
    0x428a2f98, 0x71374491, 0xb5c0fbcf, 0xe9b5dba5, 0x3956c25b, 0x59f111f1,
    0x923f82a4, 0xab1c5ed5, 0xd807aa98, 0x12835b01, 0x243185be, 0x550c7dc3,
    0x72be5d74, 0x80deb1fe, 0x9bdc06a7, 0xc19bf174, 0xe49b69c1, 0xefbe4786,
    0x0fc19dc6, 0x240ca1cc, 0x2de92c6f, 0x4a7484aa, 0x5cb0a9dc, 0x76f988da,
    0x983e5152, 0xa831c66d, 0xb00327c8, 0xbf597fc7, 0xc6e00bf3, 0xd5a79147,
    0x06ca6351, 0x14292967, 0x27b70a85, 0x2e1b2138, 0x4d2c6dfc, 0x53380d13,
    0x650a7354, 0x766a0abb, 0x81c2c92e, 0x92722c85, 0xa2bfe8a1, 0xa81a664b,
    0xc24b8b70, 0xc76c51a3, 0xd192e819, 0xd6990624, 0xf40e3585, 0x106aa070,
    0x19a4c116, 0x1e376c08, 0x2748774c, 0x34b0bcb5, 0x391c0cb3, 0x4ed8aa4a,
    0x5b9cca4f, 0x682e6ff3, 0x748f82ee, 0x78a5636f, 0x84c87814, 0x8cc70208,
    0x90befffa, 0xa4506ceb, 0xbef9a3f7, 0xc67178f2};


 void mycpy12(uint *d, const uint *s) {
#pragma unroll 3
    for (int k=0; k < 3; k++) d[k] = s[k];
}

 void mycpy16(uint *d, const uint *s) {
#pragma unroll 4
    for (int k=0; k < 4; k++) d[k] = s[k];
}

 void mycpy32(uint *d, const uint *s) {
#pragma unroll 8
    for (int k=0; k < 8; k++) d[k] = s[k];
}



 void mycpy44(uint *d, const uint *s) {
#pragma unroll 11
    for (int k=0; k < 11; k++) d[k] = s[k];
}

 void mycpy48(uint *d, const uint *s) {
#pragma unroll 12
    for (int k=0; k < 12; k++) d[k] = s[k];
}

 void mycpy64(uint *d, const uint *s) {
#pragma unroll 16
    for (int k=0; k < 16; k++) d[k] = s[k];
}

 void sha256_transform(SHA256_CTX *ctx, const uchar data[])
{
    uint a, b, c, d, e, f, g, h, i, j, t1, t2, m[64];
    uint S[8];

    //mycpy32(S, ctx->state);

    #pragma unroll 16
    for (i = 0, j = 0; i < 16; ++i, j += 4)
        m[i] = (data[j] << 24) | (data[j + 1] << 16) | (data[j + 2] << 8) | (data[j + 3]);

    #pragma unroll 64
    for (; i < 64; ++i)
        m[i] = SIG1(m[i - 2]) + m[i - 7] + SIG0(m[i - 15]) + m[i - 16];

    a = ctx->state[0];
    b = ctx->state[1];
    c = ctx->state[2];
    d = ctx->state[3];
    e = ctx->state[4];
    f = ctx->state[5];
    g = ctx->state[6];
    h = ctx->state[7];

    #pragma unroll 64
    for (i = 0; i < 64; ++i) {
        t1 = h + EP1(e) + CH(e, f, g) + dev_k[i] + m[i];
        t2 = EP0(a) + MAJ(a, b, c);
        h = g;
        g = f;
        f = e;
        e = d + t1;
        d = c;
        c = b;
        b = a;
        a = t1 + t2;
    }

    ctx->state[0] += a;
    ctx->state[1] += b;
    ctx->state[2] += c;
    ctx->state[3] += d;
    ctx->state[4] += e;
    ctx->state[5] += f;
    ctx->state[6] += g;
    ctx->state[7] += h;
}

 void sha256_init(SHA256_CTX *ctx)
{
    ctx->datalen = 0;
    ctx->bitlen = 0;
    ctx->state[0] = 0x6a09e667;
    ctx->state[1] = 0xbb67ae85;
    ctx->state[2] = 0x3c6ef372;
    ctx->state[3] = 0xa54ff53a;
    ctx->state[4] = 0x510e527f;
    ctx->state[5] = 0x9b05688c;
    ctx->state[6] = 0x1f83d9ab;
    ctx->state[7] = 0x5be0cd19;
}

 void sha256_update(SHA256_CTX *ctx, const uchar data[], int len)
{
    uint i;

    // for each byte in message
    for (i = 0; i < len; ++i) {
        // ctx->data == message 512 bit chunk
        ctx->data[ctx->datalen] = data[i];
        ctx->datalen++;
        if (ctx->datalen == 64) {
            sha256_transform(ctx, ctx->data);
            ctx->bitlen += 512;
            ctx->datalen = 0;
        }
    }
}

 void sha256_final(SHA256_CTX *ctx, uchar hash[])
{
    uint i;
    int j = 0;
    i = ctx->datalen;

    // Pad whatever data is left in the buffer.
    if (ctx->datalen < 56) {
        ctx->data[i++] = 0x80;
        while (i < 56)
            ctx->data[i++] = 0x00;
    }
    else {
        ctx->data[i++] = 0x80;
        while (i < 64)
            ctx->data[i++] = 0x00;
        sha256_transform(ctx, ctx->data);
        while(j<56)
            ctx->data[j++] = 0x00;
        // memset(ctx->data, 0, 56);
    }

    // Append to the padding the total message's length in bits and transform.
    ctx->bitlen += ctx->datalen * 8;
    ctx->data[63] = ctx->bitlen;
    ctx->data[62] = ctx->bitlen >> 8;
    ctx->data[61] = ctx->bitlen >> 16;
    ctx->data[60] = ctx->bitlen >> 24;
    ctx->data[59] = ctx->bitlen >> 32;
    ctx->data[58] = ctx->bitlen >> 40;
    ctx->data[57] = ctx->bitlen >> 48;
    ctx->data[56] = ctx->bitlen >> 56;
    sha256_transform(ctx, ctx->data);

    // Since this implementation uses little endian byte ordering and SHA uses big endian,
    // reverse all the bytes when copying the final state to the output hash.
    for (i = 0; i < 4; ++i) {
        hash[i] = (ctx->state[0] >> (24 - i * 8)) & 0x000000ff;
        hash[i + 4] = (ctx->state[1] >> (24 - i * 8)) & 0x000000ff;
        hash[i + 8] = (ctx->state[2] >> (24 - i * 8)) & 0x000000ff;
        hash[i + 12] = (ctx->state[3] >> (24 - i * 8)) & 0x000000ff;
        hash[i + 16] = (ctx->state[4] >> (24 - i * 8)) & 0x000000ff;
        hash[i + 20] = (ctx->state[5] >> (24 - i * 8)) & 0x000000ff;
        hash[i + 24] = (ctx->state[6] >> (24 - i * 8)) & 0x000000ff;
        hash[i + 28] = (ctx->state[7] >> (24 - i * 8)) & 0x000000ff;
    }
}

  void Sha256 (uchar *data, int len, uchar *hash) {
    SHA256_CTX ctx;
	sha256_init(&ctx);
	sha256_update(&ctx, data, len);
	sha256_final(&ctx, hash);
}

 /** 外部调用kernel sha256d double sha256 */
__kernel void sha256d(__global uchar* var, __global uchar *r) {
	int tid = get_local_id(0);
    int bid = get_group_id(0);
	// perform sha256 calculation here
    uint threads = core_per_sm;
    uchar hash2[48];
    uchar hash[32];
    for (int i=0; i<48; i++){
       hash2[i] = var[bid*threads*37*4+tid*37*4+24*4+i];
    }
    printf("input:%02x%02x%02x%02x",hash2[0],hash2[1],hash2[2],hash2[3]);
    Sha256(hash2, 48, hash);
    Sha256(hash, 32, hash2);
    for (int i=0; i<8; i++){
      r[bid*threads*37*4+tid*37*4+24*4+i] = hash2[i];
    //   printf("%u",bid*threads*37*4+tid*37*4+24*4+i);
    }
    printf("sha256d first 4 bytes output:%02x%02x%02x%02x",hash[0],hash[1],hash[2],hash[3]);
    printf("sha256d sencond 4 bytes output:%02x%02x%02x%02x",hash2[0],hash2[1],hash2[2],hash2[3]);
    // debug
    /*if (threadIdx.x == 0 && blockIdx.x == 0) {
      for (int i=0; i<8*32; i++) {
        printf("r[%d] is %x\n",i, ((uchar *)r)[i]);
      }
    }*/
}

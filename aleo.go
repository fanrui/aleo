package main

import (
	"bytes"
	_ "embed"
	"encoding/hex"
	"fmt"
	"os"
	"unsafe"

	"github.com/robvanmieghem/go-opencl/cl"
)

//go:embed kernel/aleo.cl
var kernelAleoSource string

const (
	ROOT_LEN          = 16 * 1024 * 8
	GEVAL_LEN         = 16 * 1024 * 12 * 4
	COINBASE_LEN      = 16 * 1024 * 8
	INITIAL_LEN       = 16 * 1024
	Evaluations_size_ = 8192
	LEN               = 12
	corespersm        = 1
)

func openclAleo() {
	// 8991af240000000000000000000000000000000000000000000000000000000000000000
	input, _ := hex.DecodeString("8991af240000000000000000000000000000000000000000000000000000000000000000")
	// fplagrange_basis_at_beta_g := make([]g1affine, INITIAL_LEN)
	// g1affineSize := unsafe.Sizeof(fplagrange_basis_at_beta_g[0])
	g1affineBin, err := os.ReadFile("lagrange_basis_at_beta_g.bin")
	if err != nil {
		fmt.Println("lagrange_basis_at_beta_g.bin不存在!", err)
		return
	}
	// for i := 0; i < INITIAL_LEN; i++ {
	// 	b := g1affineBin[i*int(g1affineSize) : (i+1)*int(g1affineSize)]
	// 	_ = json.Unmarshal(b, &fplagrange_basis_at_beta_g[i])
	// }
	// 2. Get hold of OpenCL platform and device
	platforms, err := cl.GetPlatforms()
	check("Failed to get platforms", err)
	// fmt.Println(binary.LittleEndian.Uint32(input[:4]))
	devices, err := platforms[0].GetDevices(cl.DeviceTypeAll)
	check("Failed to get devices", err)
	if len(devices) == 0 {
		panic("GetDevices returned 0 devices")
	}
	fmt.Println("Using: "+devices[0].Name(), "device count", len(devices))

	// 3. Select a device to use. On my mac: 0 == CPU, 1 == Iris GPU, 2 == GeForce 750M GPU
	context, err := cl.CreateContext([]*cl.Device{devices[0]})
	check("CreateContext failed", err)
	defer context.Release()

	// 4. Create a "Command Queue" bound to the first device
	queue, err := context.CreateCommandQueue(devices[0], 0)
	check("CreateCommandQueue failed", err)
	defer queue.Release()
	// 5. Create an OpenCL "program" from the source code.
	program, err := context.CreateProgramWithSource([]string{kernelAleoSource})
	check("CreateProgramWithSource failed", err)
	defer program.Release()

	// 5.1 Build the OpenCL program, i.e. compile it.
	err = program.BuildProgram([]*cl.Device{devices[0]}, "")
	check("BuildProgram failed", err)

	// define input buffer
	inputBuffer, err := context.CreateEmptyBuffer(cl.MemReadWrite, len(input))
	check("CreateBuffer failed for vectors input", err)
	defer inputBuffer.Release()

	input1Buffer, err := context.CreateEmptyBuffer(cl.MemReadWrite, 76)
	check("CreateBuffer failed for vectors input", err)
	defer input1Buffer.Release()

	// define root_g buffer
	rootGBuffer, err := context.CreateEmptyBuffer(cl.MemReadWrite, ROOT_LEN*4)
	check("CreateBuffer failed for vectors rootGBuffer", err)
	defer rootGBuffer.Release()

	// define geval_g buffer
	gevalGBuffer, err := context.CreateEmptyBuffer(cl.MemReadWrite, GEVAL_LEN*4)
	check("CreateBuffer failed for vectors rootGBuffer", err)
	defer gevalGBuffer.Release()

	// define root_coinbase buffer
	rootCoinbaseBuffer, err := context.CreateEmptyBuffer(cl.MemReadWrite, COINBASE_LEN*4)
	check("CreateBuffer failed for vectors rootGBuffer", err)
	defer rootCoinbaseBuffer.Release()

	// define polynomial_evaluations_e buffer
	polynomialEvaluationsEBuffer, err := context.CreateEmptyBuffer(cl.MemReadWrite, COINBASE_LEN*4)
	check("CreateBuffer failed for vectors rootGBuffer", err)
	defer polynomialEvaluationsEBuffer.Release()

	// define temp_g buffer
	tempGBuffer, err := context.CreateEmptyBuffer(cl.MemReadWrite, INITIAL_LEN*2*12*4)
	check("CreateBuffer failed for vectors rootGBuffer", err)
	defer tempGBuffer.Release()

	varBuffer, err := context.CreateEmptyBuffer(cl.MemReadWrite, 4)
	check("CreateBuffer failed for vectors input", err)
	defer varBuffer.Release()

	geVarBuffer, err := context.CreateEmptyBuffer(cl.MemReadWrite, Evaluations_size_*3*LEN*4)
	check("CreateBuffer failed for vectors input", err)
	defer geVarBuffer.Release()

	varLBuffer, err := context.CreateEmptyBuffer(cl.MemReadWrite, 4)
	check("CreateBuffer failed for vectors input", err)
	defer varLBuffer.Release()

	pInputBuffer, err := context.CreateEmptyBuffer(cl.MemReadWrite, corespersm*128*4)
	check("CreateBuffer failed for vectors input", err)
	defer pInputBuffer.Release()
	varMBuffer, err := context.CreateEmptyBuffer(cl.MemReadWrite, corespersm*Evaluations_size_*8*4)
	check("CreateBuffer failed for vectors input", err)
	defer varMBuffer.Release()
	moduleBuffer, err := context.CreateEmptyBuffer(cl.MemReadWrite, 128*4)
	check("CreateBuffer failed for vectors input", err)
	defer moduleBuffer.Release()
	resultMBuffer, err := context.CreateEmptyBuffer(cl.MemReadWrite, corespersm*48*4)
	check("CreateBuffer failed for vectors input", err)
	defer resultMBuffer.Release()
	hashBuffer, err := context.CreateEmptyBuffer(cl.MemReadWrite, 32)
	check("CreateBuffer failed for vectors input", err)
	defer hashBuffer.Release()

	// ----------------------------------------kernel hash_to_polynomial
	kernel, err := program.CreateKernel("hash_to_polynomial")
	check("CreateKernel failed", err)
	defer kernel.Release()

	err = kernel.SetArgBuffer(0, inputBuffer)
	check("SetArgBuffer 0 failed for output", err)
	// 6.1 Create OpenCL buffers (memory) for the input. Note that we're allocating 8 bytes per input element, each int64 is 8 bytes in length.

	err = kernel.SetArgBuffer(1, varBuffer)
	check("SetArgBuffer 1 failed for output", err)

	err = kernel.SetArgBuffer(2, polynomialEvaluationsEBuffer)
	check("SetArgBuffer 2 failed for output", err)
	polynomialEvaluations := make([]byte, COINBASE_LEN*4)
	if _, err = queue.EnqueueWriteBufferByte(polynomialEvaluationsEBuffer, true, 0, polynomialEvaluations, nil); err != nil {
		check("EnqueueWriteBufferByte failed", err)
	}

	inputPtr := unsafe.Pointer(&input[0])
	_, err = queue.EnqueueWriteBuffer(inputBuffer, true, 0, len(input), inputPtr, nil)
	check("EnqueueWriteBuffer inputBuffer failed", err)
	inputLen := len(input)
	inputLPtr := unsafe.Pointer(&inputLen)
	_, err = queue.EnqueueWriteBuffer(varBuffer, true, 0, 4, inputLPtr, nil)
	check("EnqueueWriteBuffer inputLBuffer failed", err)
	// 6.4 Kernel is our program and here we explicitly bind our 4 parameters to it

	// 7. Finally, start work! Enqueue executes the loaded args on the specified kernel.
	_, err = queue.EnqueueNDRangeKernel(kernel, []int{0}, []int{global_work_size}, []int{local_work_size}, nil)
	check("EnqueueNDRangeKernel failed", err)

	// 8. Finish() blocks the main goroutine until the OpenCL queue is empty, i.e. all calculations are done
	err = queue.Finish()
	check("Finish failed", err)
	if _, err = queue.EnqueueReadBufferByte(polynomialEvaluationsEBuffer, true, 0, polynomialEvaluations, nil); err != nil {
		check("EnqueueReadBufferByte failed", err)

	}
	fmt.Printf("NTTinput.bin: %x\n", polynomialEvaluations[:32])

	//----------------------------------------kernel  NTTinput.bin NTTroot.bin NTT_IO

	kernel, err = program.CreateKernel("NTT_IO")
	check("Finish failed", err)
	rootBin, err := os.ReadFile("NTTroot.bin")
	if err != nil {
		fmt.Println("NTTRoot.bin不存在!", err)
		return
	}
	err = kernel.SetArgBuffer(0, polynomialEvaluationsEBuffer)
	check("SetArgBuffer 0 failed for output", err)
	// 6.1 Create OpenCL buffers (memory) for the input. Note that we're allocating 8 bytes per input element, each int64 is 8 bytes in length.

	err = kernel.SetArgBuffer(1, rootCoinbaseBuffer)
	check("SetArgBuffer 1 failed for output", err)

	err = kernel.SetArgBuffer(2, varBuffer)
	check("SetArgBuffer 2 failed for output", err)
	// polynomialEvaluations = make([]byte, COINBASE_LEN*4)
	// if _, err = queue.EnqueueWriteBufferByte(polynomialEvaluationsEBuffer, true, 0, polynomialEvaluations, nil); err != nil {
	// 	check("EnqueueWriteBufferByte failed", err)
	// }
	if _, err = queue.EnqueueWriteBufferByte(rootCoinbaseBuffer, true, 0, rootBin, nil); err != nil {
		check("EnqueueWriteBufferByte failed", err)
	}
	// fmt.Println("xxx hash:", hex.EncodeToString(b[:32]), len(b))
	for i := 0; i < 14; i++ {
		varPtr := unsafe.Pointer(&i)
		_, err = queue.EnqueueWriteBuffer(varBuffer, true, 0, 4, varPtr, nil)
		check("EnqueueWriteBuffer inputLBuffer failed", err)
		_, err = queue.EnqueueNDRangeKernel(kernel, []int{0}, []int{512}, []int{256}, nil)
		check("EnqueueNDRangeKernel failed", err)

	}
	// ----------------------------------------kernel  bitreverse_NTT
	kernel, err = program.CreateKernel("bitreverse_NTT")
	check("bitreverse_NTT failed", err)
	err = kernel.SetArgBuffer(0, polynomialEvaluationsEBuffer)
	check("SetArgBuffer 0 failed for output", err)
	_, err = queue.EnqueueNDRangeKernel(kernel, []int{0}, []int{512}, []int{256}, nil)
	check("EnqueueNDRangeKernel failed", err)

	polynomialEvaluations = make([]byte, COINBASE_LEN*4)
	// 8. Finish() blocks the main goroutine until the OpenCL queue is empty, i.e. all calculations are done
	err = queue.Finish()
	check("Finish failed", err)
	if _, err = queue.EnqueueReadBufferByte(polynomialEvaluationsEBuffer, true, 0, polynomialEvaluations, nil); err != nil {
		check("EnqueueReadBufferByte failed", err)
	}
	//NTToutput.bin
	bb, _ := os.ReadFile("NTToutput.bin")
	for i := 0; i < len(bb); i += 64 {
		if !bytes.Equal(polynomialEvaluations[i:i+64], bb[i:i+64]) {
			fmt.Println("NTToutput not equal", i, hex.EncodeToString(polynomialEvaluations[i:i+64]), hex.EncodeToString(bb[i:i+64]))
			return
		}
	}
	fmt.Println("NTToutput.bin verify ok:", hex.EncodeToString(polynomialEvaluations[:32]))
	// ----------------------------------------kernel  repr256_to_projective384xyzz
	kernel, err = program.CreateKernel("repr256_to_projective384xyzz")
	check("CreateKernel failed", err)
	err = kernel.SetArgBuffer(0, polynomialEvaluationsEBuffer)
	check("SetArgBuffer 0 failed for output", err)
	err = kernel.SetArgBuffer(1, tempGBuffer)
	check("SetArgBuffer 1 failed for output", err)
	err = kernel.SetArgBuffer(2, gevalGBuffer)
	check("SetArgBuffer 2 failed for output", err)

	err = kernel.SetArgLocal(3, 192*32*4)
	check("SetArgBuffer 2 failed for output", err)

	// tempGPtr := unsafe.Pointer(&fplagrange_basis_at_beta_g[0])
	tempGBufferBytes := make([]byte, INITIAL_LEN*2*12*4)
	copy(tempGBufferBytes, g1affineBin)
	if _, err = queue.EnqueueWriteBufferByte(tempGBuffer, true, 0, tempGBufferBytes, nil); err != nil {
		check("EnqueueWriteBufferByte failed", err)
	}

	geVarGData := make([]byte, GEVAL_LEN*4)
	if _, err = queue.EnqueueWriteBufferByte(gevalGBuffer, true, 0, geVarGData, nil); err != nil {
		check("EnqueueWriteBufferByte geVarData failed", err)
	}
	_, err = queue.EnqueueNDRangeKernel(kernel, []int{0}, []int{8192}, []int{32}, nil)
	check("EnqueueNDRangeKernel failed", err)
	err = queue.Finish()
	check("Finish failed", err)
	// fmt.Println(2)
	geVarGData = make([]byte, GEVAL_LEN*4)
	if _, err = queue.EnqueueReadBufferByte(gevalGBuffer, true, 0, geVarGData, nil); err != nil {
		check("EnqueueReadBufferByte failed", err)
	}

	bb, err = os.ReadFile("ECNTTinput.bin")
	if err != nil {
		fmt.Println("ECNTTinput.bin不存在!", err)
		return
	}
	for i := 0; i < len(bb); i += 64 {
		if !bytes.Equal(geVarGData[i:i+64], bb[i:i+64]) {
			fmt.Println("ECNTTinput not equal", i, i/4, hex.EncodeToString(geVarGData[i:i+64]), hex.EncodeToString(bb[i:i+64]))
			return
		}
	}
	// ----------------------------------------kernel  NTT_IO_projectivexyzz
	kernel, err = program.CreateKernel("NTT_IO_projectivexyzz")
	check("CreateKernel failed", err)
	err = kernel.SetArgBuffer(0, gevalGBuffer)
	check("SetArgBuffer 0 failed for output", err)
	err = kernel.SetArgBuffer(1, rootGBuffer)
	check("SetArgBuffer 1 failed for output", err)
	err = kernel.SetArgBuffer(2, varBuffer)
	check("SetArgBuffer 2 failed for output", err)
	err = kernel.SetArgLocal(3, 192*32*4)
	check("SetArgBuffer 2 failed for output", err)

	rootGBin, err := os.ReadFile("ECNTTrootreal.bin")
	if err != nil {
		fmt.Println("ECNTTrootreal.bin不存在!", err)
		return
	}
	// rootG := make([]biginteger, 16*1024)
	// for i := 0; i < 8*1024; i++ {
	// 	for j := 0; j < 4; j++ {
	// 		// fmt.Println(i, j, (i*32)*(j*8)+8)
	// 		rootG[i].array[j] = binary.LittleEndian.Uint64(rootGBin[(i*32)+(j*8) : (i*32)+(j*8)+8])
	// 	}
	// }
	rootGData := make([]byte, ROOT_LEN*4)
	copy(rootGData, rootGBin)
	// fmt.Println("rootGBin len", len(rootGBin), ROOT_LEN*4, len(rootGData), hex.EncodeToString(rootGData[:64]), hex.EncodeToString(rootGBin[:64]))
	if _, err = queue.EnqueueWriteBufferByte(rootGBuffer, true, 0, rootGData, nil); err != nil {
		check("EnqueueWriteBufferByte failed", err)
	}
	for i := 0; i < 14; i++ {
		varPtr := unsafe.Pointer(&i)
		_, err = queue.EnqueueWriteBuffer(varBuffer, true, 0, 4, varPtr, nil)
		check("EnqueueWriteBuffer inputLBuffer failed", err)
		_, err = queue.EnqueueNDRangeKernel(kernel, []int{0}, []int{512}, []int{256}, nil)
		check("EnqueueNDRangeKernel failed", err)
		// return
	}
	err = queue.Finish()
	check("Finish failed", err)
	// ----------------------------------------kernel  bitreverse_NTT_xyzz
	kernel, err = program.CreateKernel("bitreverse_NTT_xyzz")
	check("CreateKernel failed", err)
	err = kernel.SetArgBuffer(0, gevalGBuffer)
	check("SetArgBuffer 0 failed for output", err)
	_, err = queue.EnqueueNDRangeKernel(kernel, []int{0}, []int{512}, []int{256}, nil)
	check("EnqueueNDRangeKernel failed", err)
	err = queue.Finish()
	check("Finish failed", err)

	// ----------------------------------------kernel  projectxyzz_to_neg_one
	kernel, err = program.CreateKernel("projectxyzz_to_neg_one")
	check("CreateKernel failed", err)
	err = kernel.SetArgBuffer(0, gevalGBuffer)
	check("SetArgBuffer 0 failed for output", err)
	err = kernel.SetArgBuffer(1, geVarBuffer)
	check("SetArgBuffer 1 failed for output", err)
	err = kernel.SetArgLocal(2, 192*32*4)
	check("SetArgBuffer 1 failed for output", err)
	geVarData := make([]byte, Evaluations_size_*3*LEN*4)
	if _, err = queue.EnqueueWriteBufferByte(geVarBuffer, true, 0, geVarData, nil); err != nil {
		check("EnqueueWriteBufferByte failed", err)
	}
	_, err = queue.EnqueueNDRangeKernel(kernel, []int{0}, []int{512}, []int{256}, nil)
	check("EnqueueNDRangeKernel failed", err)
	err = queue.Finish()
	check("Finish failed", err)
	geVarData = make([]byte, Evaluations_size_*3*LEN*4)
	if _, err = queue.EnqueueReadBufferByte(geVarBuffer, true, 0, geVarData, nil); err != nil {
		check("EnqueueReadBufferByte failed", err)
	}
	fmt.Println("ECNTTresultTwistedEdward_neg_one.bin:", hex.EncodeToString(geVarData[:64]))

	bb, err = os.ReadFile("ECNTTresultTwistedEdward_neg_one.bin")
	if err != nil {
		fmt.Println("ECNTTresultTwistedEdward_neg_one.bin不存在!", err)
		return
	}
	for i := 0; i < len(bb); i += 64 {
		if !bytes.Equal(geVarData[i:i+64], bb[i:i+64]) {
			fmt.Println("ECNTTresultTwistedEdward_neg_one not equal", i, i/4, hex.EncodeToString(geVarData[i:i+64]), hex.EncodeToString(bb[i:i+64]))
			return
		}
	}
	fmt.Println("cuda initial_part_program_f all verify success")

	// ----------------------------------------kernel  hash_to_polynomial_m
	kernel, err = program.CreateKernel("hash_to_polynomial_m")
	check("CreateKernel failed", err)

	// 8991af24000000000000000000000000000000000000000000000000000000000000000040c0e884b7280c5e1f27035dd4993bd574c55af9a93cf4286d4b8c4bff05ea0d0000000000000000
	input, _ = hex.DecodeString("8991af24000000000000000000000000000000000000000000000000000000000000000040c0e884b7280c5e1f27035dd4993bd574c55af9a93cf4286d4b8c4bff05ea0d0000000000000000")
	err = kernel.SetArgBuffer(0, input1Buffer)
	check("SetArgBuffer 0 failed for output", err)
	err = kernel.SetArgBuffer(1, varLBuffer)
	check("SetArgBuffer 1 failed for output", err)
	err = kernel.SetArgBuffer(2, varMBuffer)
	check("SetArgBuffer 2 failed for output", err)

	if _, err = queue.EnqueueWriteBufferByte(input1Buffer, true, 0, input, nil); err != nil {
		check("EnqueueWriteBufferByte failed", err)
	}

	varMData := make([]byte, corespersm*Evaluations_size_*8*4)
	if _, err = queue.EnqueueWriteBufferByte(varMBuffer, true, 0, varMData, nil); err != nil {
		check("EnqueueWriteBufferByte failed", err)
	}
	inputLen = len(input)
	inputLPtr = unsafe.Pointer(&inputLen)
	_, err = queue.EnqueueWriteBuffer(varLBuffer, true, 0, 4, inputLPtr, nil)
	check("EnqueueWriteBuffer inputLBuffer failed", err)
	// output2Buffer, err := context.CreateEmptyBuffer(cl.MemReadWrite, 32*8192)
	// check("CreateBuffer failed for output", err)
	// defer output2Buffer.Release()
	// err = kernel.SetArgBuffer(3, output2Buffer)
	// check("SetArgBuffer 3 failed for output", err)
	_, err = queue.EnqueueNDRangeKernel(kernel, []int{0}, []int{1}, []int{1}, nil)
	check("EnqueueNDRangeKernel failed", err)
	err = queue.Finish()
	check("Finish failed", err)
	if _, err = queue.EnqueueReadBufferByte(varMBuffer, true, 0, varMData, nil); err != nil {
		check("EnqueueReadBufferByte failed", err)

	}
	bb, err = os.ReadFile("mm.bin")
	if err != nil {
		fmt.Println("mm.bin不存在!", err)
		return
	}
	for i := 0; i < len(bb); i += 64 {
		if !bytes.Equal(varMData[i:i+64], bb[i:i+64]) {
			fmt.Println("mm.bin not equal", i, i/4, hex.EncodeToString(varMData[i:i+64]), hex.EncodeToString(bb[i:i+64]))
			return
		}
	}
	fmt.Println("mm.bin verify ok:", hex.EncodeToString(varMData[:64]))

	// ----------------------------------------kernel  TODO Verify edwardsproj_to_swaffine384
	kernel, err = program.CreateKernel("edwardsproj_to_swaffine384")
	check("CreateKernel failed", err)

	err = kernel.SetArgBuffer(0, resultMBuffer)
	check("SetArgBuffer 0 failed for output", err)
	err = kernel.SetArgBuffer(1, resultMBuffer)
	check("SetArgBuffer 1 failed for output", err)
	resultMData := make([]byte, corespersm*48*4)
	if _, err = queue.EnqueueWriteBufferByte(resultMBuffer, true, 0, resultMData, nil); err != nil {
		check("EnqueueWriteBufferByte failed", err)
	}
	fmt.Println("edwardsproj_to_swaffine384 input resultM.bin:", hex.EncodeToString(resultMData))
	_, err = queue.EnqueueNDRangeKernel(kernel, []int{0}, []int{1}, []int{1}, nil)
	check("EnqueueNDRangeKernel failed", err)
	err = queue.Finish()
	check("Finish failed", err)
	if _, err = queue.EnqueueReadBufferByte(resultMBuffer, true, 0, resultMData, nil); err != nil {
		check("EnqueueReadBufferByte failed", err)

	}
	fmt.Println("edwardsproj_to_swaffine384 output resultM.bin:", hex.EncodeToString(resultMData))

	// ----------------------------------------kernel  sha256
	kernel, err = program.CreateKernel("sha256d")
	check("CreateKernel failed", err)

	err = kernel.SetArgBuffer(0, resultMBuffer)
	check("SetArgBuffer 0 failed for output", err)

	err = kernel.SetArgBuffer(1, resultMBuffer)
	check("SetArgBuffer 1 failed for output", err)
	_, err = queue.EnqueueNDRangeKernel(kernel, []int{0}, []int{1}, []int{1}, nil)
	check("EnqueueNDRangeKernel failed", err)
	err = queue.Finish()
	check("Finish failed", err)
	if _, err = queue.EnqueueReadBufferByte(resultMBuffer, true, 0, resultMData, nil); err != nil {
		check("EnqueueReadBufferByte failed", err)
	}
	fmt.Println("sha256 resultM.bin:", hex.EncodeToString(resultMData[:192]))

}

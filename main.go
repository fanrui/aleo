package main

import (
	"encoding/binary"
	"unsafe"
)

var global_work_size = 1
var local_work_size = 1

type biginteger struct {
	array [4]uint64
}

type biginteger384 struct {
	array [6]uint64 // little endian
}

type g1affine struct {
	x biginteger384
	y biginteger384
}

func main() {

	openclAleo()

}

func check(msg string, err error) {
	if err != nil {
		panic(msg + ": " + err.Error())
	}
}

// 可以填充数组、结构体
func MemSet(s unsafe.Pointer, c byte, n uintptr) {
	ptr := uintptr(s)
	var i uintptr
	for i = 0; i < n; i++ {
		pByte := (*byte)(unsafe.Pointer(ptr + i))
		*pByte = c
	}
}

// 4字节int32 大字节序转小字节序
func conert4BytesBigEndianToLittle(b []byte) []byte {
	if len(b) != 4 {
		return b
	}
	binary.LittleEndian.PutUint32(b, binary.BigEndian.Uint32(b[:4]))
	return b
}

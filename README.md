# opencl crypto



## Support Crypto

`blake2b` `blake2s` `sha256`

## Compile && Run

- # Require go version go1.20.1 linux/amd64
- # Opencl lib
- # Display drivers
- go env -w CGO_ENABLED=1
- go mod init aleo
- go mod tidy
- go build
- ./aleo


## Result

```shell
Using: gfx1030 input:  


123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}~



Xshell
goalng blake2b hash: c3582f71ebb2be66fa5dd750f80baae97554f3b015663c8be377cfcb2488c1d1

opencl blake2b hash: c3582f71ebb2be66fa5dd750f80baae97554f3b015663c8be377cfcb2488c1d1
Using: gfx1030 input: 123
goalng sha256 hash: a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3
opencl sha256 hash: a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3
Using: gfx1030 input: hello
golang blake2s hash: 19213bacc58dee6dbde3ceb9a47cbb330b3d86f8cca8997eb00be456f140ca25
opencl blake2s hash: 19213bacc58dee6dbde3ceb9a47cbb330b3d86f8cca8997eb00be456f140ca25
Using: gfx1030 input: hello
golang blake2s hash: 324dcf027dd4a30a932c441f365a25e86b173defa4b8e58948253471b81b72cf
opencl blake2s hash: 324dcf027dd4a30a932c441f365a25e86b173defa4b8e58948253471b81b72cf

```
